package main

import "labix.org/v2/mgo"
import ("lockit"; "fmt"; "flag"; "net/http"; "os"; "runtime"; "bufio";)

//import(
	//"encoding/json"
	//"github.com/godbus/dbus"
//	"github.com/godbus/dbus/introspect"
//);

var mongo * mgo.Session;
var store map[string]*lockit.FileCache;
var repl bool;

func main(){
	parseCL();
	fixConfig();
	fmt.Println();
	db_connect();
	
	if(repl) { replLoop(); }

	lockit.Config.GlobalFileCache = make(map[string]*lockit.FileCache);
	
	/*
	conn, err := dbus.SessionBus()
	if err != nil {
		panic(err)
	}
	node, err := introspect.Call(conn.Object("org.freedesktop.systemd1", "/systemd/system/cgmanager"))
	if err != nil {
		panic(err)
	}
	data, _ := json.MarshalIndent(node, "", "    ")
	os.Stdout.Write(data)
	*/
	
	
	
	
	//s := lockit.NewDataStore();
	
	//fmt.Println(s.CreateRequestStore("/bigman", "dkaxuwjngwtpyryxzjna"));
	
	startServer();
}

func replLoop(){
	file := &lockit.File{
		Id: lockit.Config.Uid,
		Parent: "0",
		Name: "<repl>",
		Host: "Gju6Fnuy49m7",
	}
	bio := bufio.NewReader(os.Stdin)
	scr := lockit.NewScript(nil, nil, file, nil, nil, nil)
	var shouldExit bool = false;
	for !shouldExit {
		print("-> ");
		line, _, err := bio.ReadLine()
		if string(line) == "quit" || err != nil {
			break;
		}
		result := scr.RunScript(scr.CompileJS(line));
		println(result);
	}
}

func parseCL() {
	logPtr := flag.String("log", "", "log file");
	portPtr := flag.String("port", ":81", "port to listen on");
	backPtr := flag.Bool("back", false, "is behind nginx");
	realPtr := flag.Bool("real", false, "should run in real mode (real directories)");
	replMode := flag.Bool("repl", false, "should start in REPL Mode");
	flag.Parse();
	
	if *logPtr != "" {
		lockit.Config.Log = true;
		lockit.Config.LogFile = *logPtr;
	}
	
	lockit.Config.Port = *portPtr;
	lockit.Config.Behind = *backPtr;
	lockit.Config.RealMode = *realPtr;
	
	repl = *replMode;
}

func fixConfig(){
	if lockit.Config.Log {
		file, err := os.OpenFile(lockit.Config.LogFile, os.O_RDWR|os.O_APPEND|os.O_CREATE , 0660);
		if err != nil {
			fmt.Println("Error opening log file '", lockit.Config.LogFile, "'");
			os.Exit(0);
		}
		lockit.Config.LogFileH = file;
	}
	
	//Temporary fixed values
	lockit.Config.Uid = "1";
	lockit.Config.DataRoot = "/data";
	lockit.Config.TempDir = "/tmp";
	
	lockit.Config.HTTPObject = true;
	lockit.Config.FSObject = true;
	lockit.Config.DSObject = true;
	
	lockit.Config.RealModeDir = "/gospace/go/src/lockit/build.webblam.com/";
	lockit.Config.RealModeConfigFile = lockit.Config.RealModeDir + "config.csv";
	
	lockit.Config.Debug = true;
	lockit.Config.Random = lockit.NewRandomData();
}

func request(res http.ResponseWriter, req *http.Request){
	handler := &lockit.Lockit{Mongo: mongo, Res: res, Req: req};
	handler.FileCache = lockit.Config.GlobalFileCache;
	handler.Pass();
	runtime.GC();
}

func db_connect(){
	session, err := mgo.Dial("127.0.0.1");
	if err != nil{
		println("Error connecting to database!");
	}else{
		lockit.Config.Mongo = session;
		lockit.Config.FileSystem = session.DB("users").C("filesystem");
		mongo = session;
	}
}

func startServer(){
	http.HandleFunc("/", request);
	http.ListenAndServe(lockit.Config.Port, nil);
}


