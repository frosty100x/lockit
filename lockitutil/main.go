package main
import("labix.org/v2/mgo"; 
		"lockit";
		"github.com/fatih/color";
)
import ("flag"; "os"; "io"; "path/filepath"; "fmt"; "strings";)

var mongo * mgo.Session;
var command string;
var allFiles map[string]string;
var extentions map[string]string;
func main(){
	extentions = map[string]string{
		"js" : "application/javascript",
		"html" : "text/html",
		"htm" : "text/html",
		"png" : "image/png",
		"jpg": "image/jpeg",
	}
	lockit.Config.Random = lockit.NewRandomData();
	
	lockit.Config.Debug = true;
	connectToDB()

	lockit.Config.DataRoot = "/data";
	lockit.Config.Mongo = mongo;
	lockit.Config.Uid = "1";
	
	lockit.Config.FileSystem = mongo.DB("users").C("filesystem");

	//testMethod();
	
	defineFlags();
	
	switch(command){
		case "move": 			//Move files to hosting account
		moveToDB();
		case "oph":
		removeOphanedDocs();
		case "diskless":
		checkDiskLess();
		break;
	}
	
}

func defineFlags(){
	flag.StringVar(&command, "c", "", "command to preform.");
	flag.Parse();
}

func connectToDB(){
	var err error;
	mongo, err = mgo.Dial("127.0.0.1");
	if err != nil {
		println("error connecting to database. ", err);
		os.Exit(0);
	}
}

func moveToDB(){
	connectToDB();
	src := flag.Arg(0);
	dest := flag.Arg(1);
	
	if src == "" || dest == "" {
		println("[usage] -c move [source] [destination]");
		return;
	}
	
	if _, err := os.Stat(src); os.IsNotExist(err) {
   		println("Not a File or Directory.", src)
    		return;
	}
	
	initFile := &lockit.File{
		Host: "Gju6Fnuy49m7",
		Id: "0",
		Uid: "1",
	}
	
	isFirst := true;
	err := filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if strings.HasPrefix(info.Name(), ".") && !isFirst {
			println("skipped: ", path);
			if info.IsDir() {
				return filepath.SkipDir;
			}
			return nil;
		}
		
		if info.IsDir() { return nil }
		isFirst = false;
		fs := lockit.NewGoFileSystem(initFile, nil, nil);
		final := filepath.Join(dest, path);
		file := fs.Exist(final);
		
		if file == nil {
			h, err := os.Open(path);
			
			if err != nil {
				println("error @opening disk file", path);
				return nil;
			}
			created := fs.Create(final, h,  getContentType(final));
			if created != nil {
				fmt.Println("error @creating", final, created);
				return nil
			}
			println("new @", path);
		}else{
			hFile := fs.Open(final, "wt", 1024);
			
			if hFile == nil {
				println("error @opening db file", final);
				return nil;
			}
			hFile.Zero();
			
			hSrc, err := os.Open( path );
			
			if(err != nil){
				println("error @opening disk file", path); 
				fmt.Println(err);
				return nil;
			}
			
			io.Copy(hFile.Handle(), hSrc);
			println("update @", path, "\t>\t", final, "\t", file.Id);
		}
		return nil;
	});
	
	fmt.Println(err);
	//func NewGoFileSystem(m * mgo.Session, script * File, form * Array, keyvalue map[string]string) * GoFileSystem {
}

func getContentType(path string) string {
	arr := strings.Split(path, ".");
	ext := arr[len(arr)-1];
	
	contentType := extentions[ext];
	if contentType == "" {
		return "text/plain";
	}
	return contentType;
}

func isOphaned(file * lockit.File) bool {
	var last * lockit.File;
	for file != nil {
		//fmt.Println(file.Name);
		par := &lockit.File{
			Id: file.Parent,
			Uid: file.Uid,
		}
		last = file;
		file = lockit.FS.Find(par);
	}
	if last.Parent == "0"{
		return false;
	}
	return true;
}

func isOnDisk(file * lockit.File) bool {
	if file.Type != "file" {
		return false;
	}
	util := lockit.NewUtil();
	path := util.ToPath(file.Host, file.Id);
	if _, err := os.Stat(path); err == nil {
		return true;
	}
	return false;
}

func checkDiskLess() {
	var rm bool = false;
	red := color.New(color.FgRed);
	end := color.New(color.FgCyan)
	f1 := flag.Arg(0);
	if f1 == "rm" { rm = true; }
	var tmp lockit.File;
	var total int;
	
	file := &lockit.File{
		Uid: lockit.Config.Uid,
	}
	iter := lockit.FS.Iter(file);
	util := lockit.NewUtil();
	for iter.Next(&tmp){
		if tmp.Type != "file" { continue; }
		path := util.ToPath(tmp.Host, tmp.Id);
		
		if _, err := os.Stat(path); err != nil || path == "" {
			fmt.Println(tmp.Name, " ", tmp.Id, path);
			if rm {
				err = lockit.FS.Remove(&tmp, false);
				if !strings.Contains(err.Error(), "Not Found on Disk") {
					red.Println(err);
				}
			}
			total++;
		}
	}
	end.Println("Total: ", total);
}

func removeOphanedDocs(){
 	var total int = 0;
	var rm bool = false;
	f1 := flag.Arg(0);
	if f1 == "rm" { rm = true; }
	var tmp lockit.File;
	//all = make(map[string]string);
	file := &lockit.File{
		Uid: lockit.Config.Uid,
	}

	iter := lockit.FS.Iter(file);
	end := color.New(color.FgCyan)
	red := color.New(color.FgRed);
	for iter.Next(&tmp) {
		if tmp.Parent == "0" { continue; }
		if isOphaned(&tmp) {
			total++;
			fmt.Println("Ophaned: ", tmp.Name, " ", tmp.Id, "\tOnDisk: ", isOnDisk(&tmp));
			if rm {
				if err := lockit.FS.Remove(&tmp, false); err == nil {
					fmt.Println("Removed: ", tmp.Name, " ", tmp.Id);
				}else{
					red.Println("Remove Failed: ", tmp.Name, " ", tmp.Id, " ", err);
				}
			}
		}
	}
	
	end.Println("Total Ophaned:", total);
}

