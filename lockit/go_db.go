package lockit

import "labix.org/v2/mgo"
import "fmt"

type DbDefinition struct{
	Id string 						`bson:"_id,omitempty"`
	Name string					`bson:"name,omitempty"`
	Uid string						`bson:"uid,omitempty"`
	Fields [] map[string]string		`bson:"fields,omitempty"`
	Def bool						`bson:"def,omitempty"`
}

type GoDatabase struct{
	Mongo * mgo.Session
	Col * mgo.Collection
	query map[string]interface{}
	dbdef * DbDefinition
	Uid string
	dbstring string
	
	dbquery * DBQuery
}

func NewGoDatabase(m * mgo.Session, uid string) *GoDatabase{
	db := &GoDatabase{Mongo: m, Uid: uid};
	db.Init();
	return db;
}

func (d * GoDatabase) Init(){
	d.query = make(map[string]interface{});
	//d.result = make([]map[string]interface{}, 10);
	d.Col = d.Mongo.DB("userdb").C("pack1");
}

func (d * GoDatabase) toLong(name string) string{
	for _, ma := range d.dbdef.Fields {
		for k, v := range ma {
			if name == v{ return k; }
		}
	}
	return "";
}

func (d * GoDatabase) toShort(name string) string{
	for _, ma := range d.dbdef.Fields {
		for k, v := range ma {
			if name == k{ return v; }
		}
	}
	return "";
}

func (d * GoDatabase) Is(obj map[string]interface{}) {
	for mk, mv := range obj {
		k := d.toShort(mk);	
		if k == ""{ continue; }
		d.query[k] = mv;
	}
}

func (d * GoDatabase) Find() []map[string]interface{}{
	d.query["uid"] = d.Uid;
	d.query["db"] = d.dbdef.Id;
	
	result :=  make([]map[string]interface{}, 10); 		//need to fix this, result cannot be fixed at 10
	d.Col.Find(d.query).All(&result);
	arr := make([]map[string]interface{}, len(result));
	
	for i , ob := range result {
		obj := make(map[string]interface{});
		for k, v := range ob {
			if k == "uid" || k == "_id" || k == "db" || d.toLong(k) == "" { continue; }
			obj[d.toLong(k)] = v;
		}
		arr[i] = obj;
	}
	return arr;
}

func (d * GoDatabase) Create(name string, fields []string) bool{
	util := Util{};
	db := DbDefinition{Uid: d.Uid, Name: name, Def: true};
	err := d.Col.Find(db).One(&db);
	if err == nil {
		return false;
	}
	arrLength := len(fields);
	db.Fields = make([]map[string]string, arrLength);
	f := util.dbFields(arrLength);
	for i, str := range fields {
		db.Fields[i] = make(map[string]string);
		db.Fields[i][str] = f[i];
	}
	d.Col.Insert(db);
	return true;
}

func (d * GoDatabase) Insert(doc []map[string]string) int {
	var n int = 0;
	for _, doc := range doc{
		newDoc := make(map[string]string);
		for k, v := range doc {
			nk := d.toShort(k);
			
			if nk == ""{ 
				panic(d.dbdef.Name + " doesn't have field '" + k + "'"); 
			}
			newDoc[nk] = v;
		}
		newDoc["uid"] = d.Uid;
		newDoc["db"] = d.dbdef.Id;
		err := d.Col.Insert(newDoc);
		if err == nil{
			n++
		}
	}
	return n;
}
//TODO: Drop documents connected to database too.
func (d * GoDatabase) Drop() bool{
	name := d.dbdef.Name;
	db := DbDefinition{Uid: d.Uid, Name: name, Def: true};
	err := d.Col.Remove(db);
	if err != nil {
		return false;
	}
	return true;
}

func (d * GoDatabase) SetBase(base string) bool{
	d.dbstring = base;

	d.dbdef = &DbDefinition{Uid: d.Uid, Name: d.dbstring};
	err := d.Col.Find(d.dbdef).One(&d.dbdef);
	if err != nil {
		fmt.Println(err);
		return false;
	}
	return true;
}
