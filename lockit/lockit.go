package lockit

import "labix.org/v2/mgo"

import "labix.org/v2/mgo/bson"
import ("fmt"; "io/ioutil"; "os"; "bufio"; "html"; "net/http"; "time"; "mime/multipart"; "strings"; "errors"; "strconv"; "io"; )

type Lockit struct{
	Uid string
	Mongo * mgo.Session
	Res http.ResponseWriter
	Req * http.Request
	DataRoot string
	chunker * Chunker
	FileCache map[string]*FileCache;
	util Util
	scr * Script
	site * Site
	
	uploads * Array
	KeyValue map[string]string
	
	realMap []*File
}

type FileUpload struct{
	Id string
	OriginalName string
	FieldName string
	Header * multipart.FileHeader
}

func (e * Lockit) Pass(){
	/*
	 defer func() {
	 	e.Req.Body.Close();
		if r := recover(); r != nil {
			println("Recovered in pass()", r)
			e.Res.Write([]byte("internal error.\n"));
		}
	}()
	*/
	e.Uid = Config.Uid;
	e.DataRoot = Config.DataRoot;
	e.chunker = &Chunker{};
	e.util = NewUtil();
	e.uploads = NewArray();
	e.KeyValue = make(map[string]string);
	//println("Request: " + e.Req.Host + " " + e.Req.URL.Path);
	
	var host string;
	if Config.Behind {
		host = e.Req.Header.Get("x-real-host");
	}else{
		host = e.Req.Host;
	}
	
	if Config.Log {
		hFile := Config.LogFileH;
		hFile.WriteString(e.Req.Header.Get("x-real-ip") + " - "+ host + " - " + time.Now().String() + " " + e.Req.Method + " - " + 
			e.Req.URL.Path + " " + e.Req.URL.RawQuery + " - " + e.Req.UserAgent() + " - " + e.Req.Referer() + "\n");
	}

	e.site = NewSite(e.Mongo);
	
	if host != ""{
		e.site.Url = host;
	}else{
		e.Res.Write([]byte("No host!"));
		return;
	}
	e.site.Uid = e.Uid
	e.site.Split();

	if e.site.Pull() {
		go e.handleForm();
		if Config.RealMode {
			e.Res.Header().Add("RealMode", "true");
			data := e.loadCSVMap();
			file, err := e.searchConfig(data, e.Req.URL.Path);
			if err != nil{
				e.Res.Write([]byte(fmt.Sprint(err)));
				return;
			}
			e.Res.Header().Add("Content-Type", file.ReturnType);
	
			e.serveFile(file);
			return;
		}
		if e.TryCache(e.Req.URL.Path){
			return;
		}
		tra := NewTraverser2(e.Mongo, e.Req.URL.Path, e.site.Root);
		tra.IsAbs(false);
		e.getContent(tra);
	}else{
		e.Res.Write([]byte("No host!"));
	}
}

func (e * Lockit) searchConfig(arr * Array, path string) (*File, error) {
	file := arr.Pop();
	
	for file != nil {
		_file := file.(*File);	
		if _file.Name.(string) == path {
			return _file, nil;	
		}
		file = arr.Pop();
	}
	return nil, errors.New("Path not in config file.");
}

func (e * Lockit) loadCSVMap() * Array {
	var path string = Config.RealModeConfigFile;
	files := NewArray()
	
	cont, err := ioutil.ReadFile(path);
	
	if err != nil {
		println("Could not load config file @"+ path);
		os.Exit(0);
	}
	
	lines := strings.Split(string(cont), "\n");
		
	for _, line := range lines {
		if line == "" || (line[0] == '/' && line[1] == '/') { 
			continue;	
		}
		fields := strings.Split(line, ",");
		file := &File{
			Name: fields[0],
			Ext: fields[1],
			ReturnType: fields[2],
			Parent: "xvqbhtqzlxzxjrqlfehj",
			Host: "Gju6Fnuy49m7",
		}
		files.Push(file);
	}
	return files;
}

func ( e * Lockit) handleForm(){
	rand := NewRandomData();
	err := e.Req.ParseMultipartForm(1024*100);
	
	ct := e.Req.Header["Content-Type"];
	
	parsed := e.Req.ParseForm();

	if e.Req.PostForm != nil && parsed == nil {
		for k, v := range e.Req.PostForm { e.KeyValue[k] = v[0]; }
	}/*else{
		fmt.Println(parsed);
	}*/
	
	if len(ct) <= 0 || ct[0] != "multipart/form-data" { return; }
	if err == nil {
		form := e.Req.MultipartForm;
		if form != nil {
			for k, v := range form.File{
				for _, header := range v {
					upload := &FileUpload{
						Id: rand.Random(50),
						FieldName: k,
						Header: header,
					};
					e.uploads.Push(upload);
				}
			}
			for k, v := range form.Value{ e.KeyValue[k] = v[0]; }
		}
	
	}else{
		fmt.Println("Error Parsing Form Data.", err);
	}
}

func (e * Lockit) checkCache(name string) bool{
	for na, _ := range e.FileCache {
		//println("comparing: -"+ na + " - " + name) ;
		if(na == name){
			//println(na, " in cache!");
			//e.Res.Write([]byte("testing"));
			//e.serveStorage( file );
			//e.scr.RunScript( e.Test );
			return true;
		}
	}
	//println("not in cache!");
	return false;
}

func (e * Lockit) e(s string){
	if Config.Debug {
		println(s);	
	}
}

func (e * Lockit) TryCache(name string) bool {
	cache := e.FileCache[name];
	if cache == nil { return false; }
	e.Res.Header().Add("Content-Type", "text/html");
	e.Res.WriteHeader(200);
	
	e.Res.Header().Add("Cache", "true");
	sto := NewStorage(cache, e.Res, e.Req, e.uploads, e.KeyValue);

	n, err := io.Copy(e.Res, sto);
	if err != nil && fmt.Sprint(err) != "EOF"{
		fmt.Println("Written:", err, n);
	}
	return true;
}

func(e * Lockit) serveFile(file * File) {
	if e.TryCache(file.Name.(string)){ return; }
	
	var path string;
	if Config.RealMode {
		path = Config.RealModeDir + file.Name.(string);	
	}else{
		path = e.util.ToPath(file.Host, file.Id);	
	}
	if file.Ext == "js" {
		cont, err := ioutil.ReadFile(path);
		if err == nil {
			script := NewScript(e.Res, e.Req, file, e.uploads, e.KeyValue, nil);
			script.Run(string(cont), false);
			script = nil;
		}else{
			e.Res.Write([]byte("disk error!"));
			e.e("disk error: @"+ path); 
		}
	}else if file.Ext == "m-js" {
		e.scr = NewScript(e.Res, e.Req, file, e.uploads, e.KeyValue, nil);
		//script.SetHostId( file.Host );
		e.scr.SetHostId( file.Host );
		e.sendFile(path);
		//e.serveAndCache(path, file.Id, false);
		
	}else{
		cont, err := ioutil.ReadFile(path)
		if err == nil{
			e.Res.Write(cont);
			return;
		}else{
			e.Res.Write([]byte("disk error"));
			e.e("disk error: @" + path);
		}
	}
}

func (e * Lockit) sendFile(path string) {
	f, err := os.Open(path);
	if err != nil {
		fmt.Println(err);
	}
	io.Copy(e.Res, f);
}

func ( e * Lockit) isOld(file * File) bool {
	if file.ModifiedDate == 0 { FS.UpdateModTime(file); }
	modedate := e.Req.Header.Get("if-none-match");
	if modedate != "" {
		if modedate == strconv.Itoa(file.ModifiedDate) {
			return true;
		}
		return false;
	}else{
		e.Res.Header().Add("etag", strconv.Itoa(file.ModifiedDate));
	}
	return false;
}

//Temporary function
func (e * Lockit) shouldIgnore(file * File) bool {
	ignore := []string{
		"storage.js", "main.css", "shortcuts.js", "fs.js", "app.js", "m_login.js",
		"v_login.js",
	}
	for _, v := range ignore {
		if file.Name == v{ return true; }
	}
	return false;
}

func(e * Lockit) getContent(tra * Traverser2){
	var file * File = tra.Next2();
	for file != nil {
		if file.Type == "file" {
			if !e.shouldIgnore(file) && e.isOld(file){
				e.Res.WriteHeader(304); return;
			}
			e.Res.Header().Add("Content-Type", file.ReturnType);
			e.serveFile(file); return;
		}else if file.Type == "dir" && tra.EOD() {
			e.serveDefault(file); return;
		}
		file = tra.Next2();
	}
	e.Res.WriteHeader(404);
}

func (e * Lockit) serveDefault(file * File){
	if file.Default != "" {
		def := &File{
			Id: file.Default,
			Uid: e.Uid,
			Host: file.Host,
		}
		def = FS.Find(def);
		if def == nil {
			e.Res.Write([]byte("Config Error"));
			return;
		}
		e.Res.Header().Add("Content-Type", def.ReturnType);
		e.serveFile(def);
	}else{
		def := &File{
			Uid: e.Uid,
			Parent: file.Id,
			Host: file.Host,
			Name:  bson.M{"$in" : e.site.Defaults},
		}
		def = FS.Find(def);
		if def != nil {
			e.serveFile(def);
		}else{
			e.Res.Write([]byte("Directory has no default."));
		}
	}
}

func (e * Lockit) serveAndCache(path, id string, cache bool){
	f, err := os.Open(path);
	if err != nil {
		fmt.Println(err);
		return;
	}
	reader := bufio.NewReader(f);
	//sto := NewStorage(nil, e.scr);
	
	var pos int = 0;
	
	NewParser(func(text []byte, _type string, evaltype string){
		if _type == "html" {
			//sto.AddText(string(text));
			pos += len(text);
			//fmt.Print(string(text));
			e.Res.Write(text);
		}
		if _type == "js" {
			scr := e.scr.CompileJS(text);
			//sto.AddScript(scr, evaltype);
			result := e.scr.RunScript( scr );
			if evaltype == "dash" {
				e.Res.Write([]byte(html.EscapeString(result)));
			}else if evaltype == "equal" {
				e.Res.Write([]byte(result));
			}
		}
	}, NewChunker(reader, 1024));
	//e.FileCache[id] = sto;
	//fmt.Println( string(sto.text) );
}

func (e * Lockit) serveStorage(g * Storage){
	lastPos := 0;
	for i, script := range g.Scripts() {
		var n int = lastPos;
		
		//e.Res.Write( g.text[lastPos: i] );
		for n<i {
			//println(n, "-", string(g.Text[n]));
			e.Res.Write([]byte(string(g.cache.buff[n])));
			n++;
		}
		//fmt.Println(i, " ", script);
		//v := NewScript(e.Mongo, e.Res, e.Req, e.Uid, "testfile.js");
		//sr := e.scr.CompileJS(script);
		result := e.scr.RunScript(script);
		if g.ScriptType(i) == "dash" {
			e.Res.Write([]byte(html.EscapeString(result)));
		}else if g.ScriptType(i) == "equal" {
			e.Res.Write([]byte(result));
		}
		lastPos = n;
	}
	
	//e.Res.Write( e.text[lastPos: len(g.text)];
	for lastPos < len(g.cache.buff) {
		e.Res.Write([]byte(string(g.cache.buff[lastPos])));
		lastPos++
	}
}