package lockit

import "github.com/idada/v8.go"
import ("os"; "fmt"; "bufio"; "errors";)

type GoDataStore struct{
	engine * v8.Engine
}

func NewGoDataStore() * GoDataStore{
	s := &GoDataStore{};
	s.Init();
	return s;
}

func (d * GoDataStore) Init(){}

func (d * GoDataStore) CreateURLStore(path string, fid string) error {
	file := &File{
		Id: fid,
		Uid: Config.Uid,
	};
	
	result := FS.Find(file);
	if result == nil || result.Type == "dir" { 
		fmt.Println(result);
		return errors.New("File does not exist");
	}
	
	fmt.Println(result)
	
	util := NewUtil();
	fpath := util.ToPath(result.Host, result.Id);
	
	f, err := os.Open(fpath);
	if err != nil {
		fmt.Println(err);
		return errors.New("Could not open file "+ path);
	}
	reader := bufio.NewReader(f);
	sto := NewStorage(nil, nil, nil, nil, nil);
	engine := sto.Engine();
	
	NewParser(func(text []byte, _type string, evaltype string){
		if _type == "html" {
			sto.AddText(string(text));
		}
		if _type == "js" {
			scr := engine.Compile(text, nil);
			sto.AddScript(scr, evaltype);
		}
	}, NewChunker(reader, 1024));
	
	sto.Commit(path);
	return nil;
}
