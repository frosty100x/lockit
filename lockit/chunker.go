package lockit

import "bufio"

type Chunker struct{
	size int
	reader * bufio.Reader
	buff []byte;
	pos int ;
	rpos int;
}

func NewChunker(reader * bufio.Reader, size int) * Chunker{
	ch := &Chunker{};
	ch.Init(reader, size);
	return ch;
}

func (c * Chunker) Init(reader * bufio.Reader, size int){
	c.buff = make([]byte, size);
	c.size = size;
	c.reader = reader;
	c.pos = c.size+1;
	c.rpos = 0;
}

func (c * Chunker) Next() byte{
	if c.pos >= c.size{
		c.buff = make([]byte, c.size);		//We need look into "zeroing" out buffer instead.
		i, err := c.reader.Read(c.buff);
		if err != nil || i ==0 { return 0; }
		c.pos = 0;
	}
	b := c.buff[c.pos];
	if b != 0 { c.pos++; }
	c.rpos++;
	return b
}

func (c * Chunker) Peek() byte {
	if(c.pos >= c.size){
		buff, err := c.reader.Peek(1);
		if err != nil{ return 0 };
		return buff[0];
	}
	return c.buff[c.pos];
}

func (c * Chunker) Position() int {
	return c.rpos;
}
