package lockit

import ("testing"
		"labix.org/v2/mgo"
		"os"
);

var default_uid = "1";
var mongo * mgo.Session;
func init(){
	connectToDB();
}

func AreEqual(a, b interface{}, t * testing.T){
	t.Fatal("Expected: ", a, " Actual: ",  b);
}

func IsTrue(v bool, t * testing.T){
	if !v { 
		t.Fatal("Value is not true.");
	}
}

func IsFalse(v bool, t * testing.T){
	if v{
		t.Fatal("Value is not false.");
	}
}


func connectToDB(){
	var err error;
	mongo, err = mgo.Dial("127.0.0.1");
	if err != nil {
		println("error connecting to database. ", err);
		os.Exit(0);
	}
}

func Test_ArrayContains(t *testing.T) {
	arr := NewArray();
	
	arr.Push("fuzz");
	arr.Push("fuzzy Ring");
	arr.Push(50);
	
	IsTrue(arr.Contains("fuz"), t);
	IsFalse(arr.Contains("grapefruit"), t);
	IsTrue(arr.Contains(50), t);
}


