package lockit

//import "labix.org/v2/mgo/bson"
import "labix.org/v2/mgo"
//import "fmt"

type Account struct{
	mongo * mgo.Session
	coll * mgo.Collection
	Pw string `bson:"pw,omitempty"`
	Username string `bson:"username"`
}

func NewAccount(session * mgo.Session) *Account{
	coll := session.DB("users").C("accounts");
	return  &Account{mongo: session, coll: coll};
}

func (e * Account) Get(){
	err := e.coll.Find(e).One(&e);
	if err != nil {
		//println(err)
		panic(err);
	}
}
