package lockit

import "github.com/idada/v8.go"
import "labix.org/v2/mgo"
//import "fmt"

type Database struct{
	Mongo * mgo.Session
	Col * mgo.Collection
	Uid string
	//Script * v8.Script
	engine *v8.Engine
	Context *v8.Context
	
	query map[string]interface{}
	result [] map[string]interface{}
	dbstring string
	pack string "pack1"
	
	db * GoDatabase
	funcTemp map[string]*v8.FunctionTemplate
}

type DBQuery struct{
	engine *v8.Engine
	Context *v8.Context
	
	js_self *v8.ObjectTemplate
	
	db * GoDatabase
	funcTemp map[string]*v8.FunctionTemplate
}

func (d * Database) Init(en *v8.Engine){
	d.engine = en;
	d.db = NewGoDatabase(d.Mongo, d.Uid);
	d.query = make(map[string]interface{});
	d.result = make([]map[string]interface{}, 10);
	d.funcTemp = make(map[string]*v8.FunctionTemplate, 5);
	
	d.funcTemp["base"] = d.engine.NewFunctionTemplate(d.base, nil);
	d.funcTemp["bases"] = d.engine.NewFunctionTemplate(d.bases, nil);
	d.funcTemp["create"] = d.engine.NewFunctionTemplate(d.create, nil);
	
	//d.funcTemp["drop"] = d.engine.NewFunctionTemplate(d.drop, nil);
	//d.funcTemp["is"] = d.engine.NewFunctionTemplate(d.is, nil);
	//d.funcTemp["not"] = d.engine.NewFunctionTemplate(d.not, nil);
	//d.funcTemp["find"] = d.engine.NewFunctionTemplate(d.find, nil);
	//d.funcTemp["insert"] = d.engine.NewFunctionTemplate(d.insert, nil);
	
	d.Col = d.Mongo.DB("userdb").C("pack1");
}

func (d * DBQuery) SetJSFunctions(){
	d.funcTemp = make(map[string]*v8.FunctionTemplate);
	d.funcTemp["drop"] = d.engine.NewFunctionTemplate(d.drop, nil);
	d.funcTemp["is"] = d.engine.NewFunctionTemplate(d.is, nil);
	d.funcTemp["not"] = d.engine.NewFunctionTemplate(d.not, nil);
	d.funcTemp["find"] = d.engine.NewFunctionTemplate(d.find, nil);
	d.funcTemp["insert"] = d.engine.NewFunctionTemplate(d.insert, nil);
}

func (d * DBQuery) Accessor(name string, info v8.AccessorCallbackInfo) {
	info.ReturnValue().Set(d.funcTemp[name].NewFunction());
}

func (d * Database) Accessor(name string, info v8.AccessorCallbackInfo) {
	info.ReturnValue().Set(d.funcTemp[name].NewFunction());
}

func (d * DBQuery) Init(){
	d.funcTemp = make(map[string]*v8.FunctionTemplate);
}

func (d * Database) bases(info v8.FunctionCallbackInfo){
	en := d.engine
	query := make(map[string]interface{});
	var result []map[string]interface{};
	query["uid"] = d.Uid;
	query["def"] = true;
	d.Col.Find(query).All(&result);
	
	length := len(result);
	arr := en.NewArray(length);

	for n := 0; n < length; n++ {
		arr.ToArray().SetElement(n, en.NewString( result[n]["name"].(string)) );
	}
	info.ReturnValue().Set( arr );
}

func (d * DBQuery) is(info v8.FunctionCallbackInfo) {
	en := d.engine
	key := info.Get(0).String();
	value := info.Get(1).String();
	
	funcIs := make(map[string]interface{});
	funcIs[key] = value;
	d.db.Is(funcIs);
	
	info.ReturnValue().Set(en.NewInstanceOf(d.js_self));
}

func (d * DBQuery) not(info v8.FunctionCallbackInfo){
	//en := d.engine;
	
}

func (d * DBQuery) insert(info v8.FunctionCallbackInfo) {
	en := d.engine;
	obj := info.Get(0).ToObject();
	/*
	tmpInsert := make(map[string]string);
	names := obj.GetPropertyNames();
	
	for n := 0; n < names.Length(); n++ {
		key := names.GetElement(n).String();
		value := obj.GetProperty(key);
		short := d.toShort(key);
		if short != "" && !(value.IsObject() || value.IsFunction()){
			tmpInsert[short] = value.String();
		}
	}
	if len(tmpInsert) < 1 {
		info.ReturnValue().Set( en.False() );
		return; 
	}
	
	tmpInsert["uid"] = d.Uid;
	tmpInsert["db"] = d.dbdef.Id;
	err := d.Col.Insert( tmpInsert );
	*/
	
	names := obj.GetPropertyNames();
	objLen := names.Length();
	tmpInsert := make([]map[string]string, objLen);
	
	for n := 0; n < objLen; n++ {
		key := names.GetElement(n).String();
		value := obj.GetProperty(key);
		tmpInsert[n][key] = value.String();
	}
	
	count := d.db.Insert(tmpInsert);
	
	info.ReturnValue().Set( en.NewInteger(int64(count)) );
	
	/*
	if err != nil {
		info.ReturnValue().Set( en.False() );
	}else{
		info.ReturnValue().Set( en.True() );
	}
	*/
}

func (d * DBQuery) drop(info v8.FunctionCallbackInfo){
	en := d.engine;
	result := d.db.Drop();
	
	if result {
		info.ReturnValue().Set(en.True() );
	}else{
		info.ReturnValue().Set( en.False() );
	}
	
	/*
	name := d.dbdef.Name;
	
	db := DbDefinition{Uid: d.Uid, Name: name, Def: true};
	err := d.Col.Remove(db);
	if err != nil {
		info.ReturnValue().Set( en.False() );
		return;
	}
	info.ReturnValue().Set( en.True() );
	*/
}

func (d * Database) create(info v8.FunctionCallbackInfo){
	en := d.engine;
	name := info.Get(0);
	fields := info.Get(1);
	
	if !name.IsString() || !fields.IsArray(){
		//info.ReturnValue().Set( en.Null() );
		panic("wrong params passed to create");
		return;
	}
	arr := fields.ToArray();
	createFields := make([]string, arr.Length());
	
	for n := 0; n < arr.Length(); n++ {
		createFields[n] = arr.GetElement(n).String();
	}
	result := d.db.Create(name.String(), createFields);
	if result {
		info.ReturnValue().Set( en.True() );
	}else{
		info.ReturnValue().Set( en.False() );
	}
	
	/*
	db := DbDefinition{Uid: d.Uid, Name: name.String(), Def: true};
	
	err := d.Col.Find(db).One(&db);
	if err == nil {
		info.ReturnValue().Set( en.False() );
		return;
	}
	
	arr := fields.ToArray();
	db.Fields = make([]map[string]string, arr.Length());
	f := util.dbFields(arr.Length());
	for n := 0; n < arr.Length(); n++ {
		db.Fields[n] = make(map[string]string);
		db.Fields[n][arr.GetElement(n).String()] = f[n];
	}
	d.Col.Insert(db);
	info.ReturnValue().Set( en.True() );
	*/
}

func (d * DBQuery) find(info v8.FunctionCallbackInfo)  {
	en := d.engine;
	/*
	d.db.query["uid"] = d.Uid;
	d.db.query["db"] = d.dbdef.Id;
	d.db.Col.Find(d.query).All(&d.result);
	arr := d.db.engine.NewArray(len(d.result));
	
	for i , ob := range d.result {
		js_obj := d.engine.NewObjectTemplate();
		for k, v := range ob {
			if k == "uid" || k == "_id" || k == "db" || d.toLong(k) == "" { continue; }
			js_obj.SetProperty(d.toLong(k), d.engine.NewString(v.(string)), v8.PA_None);
		}
		arr.ToArray().SetElement(i, d.db.engine.NewInstanceOf(js_obj));
	}
	*/
	result := d.db.Find();
	arr := d.engine.NewArray(len(result));
	
	for i, doc := range result {
		js_obj := d.engine.NewObjectTemplate();
		
		for k, v := range doc {
			js_obj.SetProperty(k, en.NewString(v.(string)), v8.PA_None);
		}
		arr.ToArray().SetElement(i, d.engine.NewInstanceOf(js_obj));
	}
	
	info.ReturnValue().Set( arr );
}

func (d * Database) base(info v8.FunctionCallbackInfo) {
	en := d.engine;
	var name string;
	if !info.Get(0).IsString() {
		panic("base excepts string");
	}
	name = info.Get(0).String();
	
	db := NewGoDatabase(d.Mongo, d.Uid);
	
	if !db.SetBase(name){
		panic("no such base " + name);
		return;
	}

	obj := &DBQuery{engine: d.engine};
	obj.SetJSFunctions();
	obj.db = db;
	
	info.ReturnValue().Set(en.NewInstanceOf(d.QueryObject(obj)));
}

func (d * Database) QueryObject(dbquery * DBQuery) *v8.ObjectTemplate {
	//data := d.GetFuncTemp(db);
	//qobj := &DBQuery{Mongo: d.Mongo, Uid: d.Uid};
	
	obj := d.engine.NewObjectTemplate();
	obj.SetAccessor("is", dbquery.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("not", dbquery.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("find", dbquery.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("insert", dbquery.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("drop", dbquery.Accessor, nil, nil, v8.PA_None);
	return obj;
}

func (d * Database) BaseObject() *v8.ObjectTemplate{
	obj := d.engine.NewObjectTemplate()
	
	obj.SetAccessor("base", d.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("bases", d.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("create", d.Accessor, nil, nil, v8.PA_None);
	return obj;
}

func InstanceDB(en * v8.Engine, m * mgo.Session, uid string) * v8.ObjectTemplate{
	db := &Database{Uid: uid, Mongo: m};
	db.Init(en);
	return db.BaseObject();
}
