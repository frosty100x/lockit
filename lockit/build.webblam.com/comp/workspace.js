define(['backbone', 'app.js', 'comp/v_TreeView', 'comp/m_TreeModel', 'comp/v_Editor', 'comp/m_Editor', 'comp/c_Tree', 'comp/DockWindow'], 
function(Backbone, app, TreeView, TreeModel, EditorView, EditorModel, TreeModelCollection, DockWindow){
	
	Workspace = Backbone.Marionette.LayoutView.extend({
		template: '#workspace-templete',
		initialize: function(){
			//this.defaultParams = {x: 50, y: 50, width: 300, height: 350, name: 'Window Header'};
			this.windows = [];
			_.bindAll(this, 'onShow');
			//this.dock = new DockWindow();
			var editor = new EditorView({
					model: new EditorModel()
			});
			this.windows.push(editor);
			var initData = [
				{name: 'root', id: '0', type: 'dir'}
			]
			var treeView = new TreeView({
				model: new TreeModel({id: '0', name: 'root', type:'dir'}),
				collection: new TreeModelCollection()
			});
			this.windows.push(treeView);
		},
		
		showAll: function(){
			var self = this;
			_.each(this.windows, function(window){
				window.render();
				var obj = window.view();
				obj.addClass('app_window');
				self.$el.append(obj);
			});
		},
		
		onShow: function(){
			this.showAll() 
		}
	
	});
	return Workspace;

});
