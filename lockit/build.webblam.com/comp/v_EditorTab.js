define(['backbone', 'comp/v_EditorTabItem', 'comp/c_EditorTabItem'], function(Backbone, EditorTabItemView, EditorTabItemCollection){
	EditorTabView = Backbone.Marionette.CollectionView.extend({
		//template: "editor-tabs-bar-template",
		childView: EditorTabItemView,
		//tagName: 'div',
		initialize: function(options){
			this.collection = new EditorTabItemCollection(options);
			this.render();
		}
	});
	return EditorTabView;	
});
