define(['backbone', 'comp/m_EditorTabItemModel'], function(Backbone, EditorTabItemModel){
	EditorTabItemCollection = Backbone.Collection.extend({
		model: EditorTabItemModel
	});
	return EditorTabItemCollection;	
});
