define(['backbone', 'app.js'], function(Backbone, app){
	EditorTabItemView = Backbone.Marionette.ItemView.extend({
		template: "#editor-tab-item-template",
		tagName: 'span',
		initialize: function(options){
			//this.name = options.name;
			//this.selected = options.selected;
			//this.model.name = "Greg (model)";
		},
		
		onRender: function(){
			if(this.model.get('selected'))
				this.$el.find('span').addClass("editor-tab-item-selected", 1000, 'linear');
		},
		
		click: function(){
			if(!this.model.get('selected'))
				app.trigger('file:click', this.model.get('id'));
		},
		
		events: {
			'click': 'click'
		}
	});
	return EditorTabItemView;	
});
