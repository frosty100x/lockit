define(['backbone', 'app.js', 'modules/shortcuts', 'modules/fs', 'modules/storage'], function(Backbone, app, fs, store){
	EditorModel = Backbone.Model.extend({
		loadedFiles: [],
		selectedFile: null,
		selectedFileContent: null,
		initialize: function(){
			var self = this;
			this.loadedFiles = [];
			//app.on('file:show', function(data, _id){ self.fileReceived(data, _id); })
			app.on('file:click', this.attemptFileOpen, this);
			//app.on('action:file:save', this.saveActiveFile, this);
			//app.on('file:show', this.showCalled, this);
		},
		
		showFile: function(content, row){
			console.log('showing', row);
			this.selectedFile = row;
			this.selectedFileContent = content;
		},
		
		saveFile: function(content){
			console.log("Saving", this.selectedFile.id, this.selectedFile.name, this.selectedFileContent);
			if(this.selectedFile == null || content == null){
				console.log("Error saving file!");
			}
			app.fs.saveFile(this.selectedFile.id, content, function(result){
				console.log('Save result: ', result);
			});
		},
		
		attemptFileOpen: function(id){
			var fileObj = app.store.fileObject(id);
			if(fileObj != null){
				//var content = fileObj[0], row = fileObj[1];
				console.log("Local: ", fileObj.row.name, fileObj.row.id);
				this.addToLoaded(fileObj.row);
				this.trigger('file:show', fileObj.content, fileObj.row, this.selectedFileContent, this.selectedFile);
				this.selectedFile = fileObj.row;
				this.selectedFileContent = fileObj.content;
			}else{
				this.loadFile(id);
			}
		},
		
		getOpenFiles: function(){
			var rows = this.loadedFiles;
			for(i=0; i < rows.length; i++)
				if(rows[i].id == this.selectedTab)
					rows[i].selected = true;
			return rows;
		},
		
		loadFile: function(id){
			var self = this;
			app.fs.fileObject(id, function(_data, _row){
				console.log('Remote', _row.name, _row.id);
				self.fileLoaded(_data, _row);
			});
		},
		
		fileLoaded: function(content, row){
			this.addToLoaded(row);
			
			this.trigger('file:show', content, row,  this.selectedFileContent, this.selectedFile);
			this.selectedFile = row;
			this.selectedFileContent = content;
		},
		/*
		fileReceived: function(data, id){
			var self = this;
			var row = app.store.fileRow(id);
			
			if(row === null){
				app.fs.fileRow(id, function(id, _row){
					self.addToLoaded(_row, data);
				});
			}
			else{
				self.addToLoaded(row, data);
			}
		},
		*/
		isLoaded: function(id){
			for(i=0; i<this.loadedFiles.length; i++){
				//console.log('Comparing:', this.loadedFiles[i].id, id, (this.loadedFiles[i].id == id));
				if(this.loadedFiles[i].id == id) return true;
			}
			return false;
		},
		
		updateActiveFileCache: function(content){
			app.store.addFileContent(this.selectedFile.id, content);
		},
		
		addToLoaded: function(row, data){
			if(!this.isLoaded(row.id)) {
				//if(this.loadedFiles.length < 1)
				//	row._selected = true;
				this.loadedFiles.push(row);
			}
			//this.trigger('sync:loadedFiles', row, data);
		},
	
		events: {
			'file:loaded': 'fileLoaded'
		}
	});
	return EditorModel;
});
