define(['backbone', 'app.js'], function(Backbone, app){	
	TreeModel = Backbone.Model.extend({
		initialize: function(){
		},
		
		openfile: function(id){
			var self = this;
			var content = app.store.fileContent(id);
			
			if(content === null){
				app.fs.fileContent(id, function(data, id){
					console.log('Remote:', id);
					app.trigger('file:show', data, id);
				});
			}else{
				console.log('Local:', id);
				app.trigger('file:show', content, id);
			}
		},
		
		getFiles: function(){
			//app.fileById(this.id);
		},
		
		url: function(){
			return url + "dir?type=dir:list&id=" + this.get('id');
		},
		
		open: function(){
			app.trigger('file:click', this.get('id'));
		},
		
		triggerFileOpen: function(_id){
			this.fs.fileContents(_id);
		},
		
		defaults: {
			isOpen: false
		}
	});
	return TreeModel;
});
