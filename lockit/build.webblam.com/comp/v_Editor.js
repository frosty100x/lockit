define(['backbone', 'ace/ace', 'app.js', 'comp/v_EditorTab', 'comp/DockWindow'], 
 function(Backbone, ace, app, EditorTabView, DockWindow){
	EditorView = Backbone.Marionette.ItemView.extend({
		template: "#editor-template",
		self: this,
		initialize: function(){
			var s = this;
			var self = this;
			//this.tabsTemplate = _.template('<% _.each(tabs, function(item, index){ %> <span class="editor-tab-item"> <%= item.name %> </span> <% }); %>');
			this.model.on('file:show', this.showFile, this);
			app.on('action:file:save', this.saveActiveFile, this);
		},
		
		saveActiveFile: function(){
			this.model.saveFile(this.getContent());
		},
		
		showFile: function(content, row, oldContent, oldRow){
			if(oldRow != null) this.updateActiveFileCache(oldRow.id, this.getContent());
			this.setContent(content);
			this.renderTabBar(this.model.getOpenFiles(), row.id);
		},
		
		fileShouldLoad: function(row, content){
			this.setContent(content);
			this.renderTabBar(this.model.loadedFiles);
		},
		
		updateActiveFileCache: function(id, content){
			app.store.addFileContent(id, content);
		},
		
		renderTabBar: function(rows, id){
			rows = this.setSelected(rows, id);
			var bar = this.$el.find("#editor-tab-bar");
			var tabs = new EditorTabView(rows);
			bar.empty().append(tabs.el);
		},
		
		setSelected: function(rows, id){
			for(i=0; i < rows.length; i++)
				if(rows[i].id === id) rows[i].selected = true;
				else rows[i].selected = false;
			return rows;
		},
		
		onRender: function(){
			var self = this;
			
			this._initialize({
				dragHandle: self.$el.find('#editor_header'),
				resizeHandle: self.$el.find('#editor_resize')
			});
			this.$el.css("height", "100%");
			var obj = this.$el.find("#editor");
			var editor = ace.edit(obj[0]);
			
			editor.setOptions({
				maxLines: 100
			});
			editor.setTheme("ace/theme/monokai");
			editor.getSession().setMode("ace/mode/javascript");
			this.$el.find("#editor").css("height", "100%");
			this.editor = editor;
		},
		
		setContent: function(data){
			this.editor.setValue(data);
			this.editor.clearSelection();
		},
		
		getContent: function(){
			return this.editor.getValue();
		},
		
		view: function(){
			this.render();
			return this.$el;
		},
		
		openfile: function(id){
			if(this.model.isLoaded(id))
				this.show(id);
			else
				this.loadFile(id);
		}
	}).extend(DockWindow);
	return EditorView;	
});
