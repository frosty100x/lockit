define(['backbone', 'comp/m_TreeModel'], function(Backbone, TreeModel){
	TreeModelCollection = Backbone.Collection.extend({
		model: TreeModel
	});
	return TreeModelCollection;
});
