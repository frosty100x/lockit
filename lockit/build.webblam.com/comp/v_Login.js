define(['backbone'], function(Backbone){
	LoginView = Backbone.Marionette.ItemView.extend({
		tagName: 'div',
		template: "#login-template",
		initialize: function(){
			console.log("Login View init");
		}
	});
	return LoginView;
});
