define(['backbone', 'app.js', 'modules/util', 'libs/jquery-ui.min'], function(Backbone, app){
/*
	DockWindow = Backbone.Marionette.Controller.extend({
		template: '#dockwindow',
		dragging: false,
		down: false,
		intx: 0, inty: 0, intWidth: 0, intHeight: 0,
		initialize: function(params){
			this.objects = {};
			this.active = null;
			this.window_default_css = {
				//'background-color': 'white',
				'border-radius': '5px',
				//'border' : '1px black solid',
				'padding' : '5px'
			};
			$('html').mouseup($.proxy(this.mouseup, this));
			$('html').mousemove($.proxy(this.move, this));
			/*
			this.bounds = {
				topL: {x:0, y:0}, 
				bottomR: {x:500, y:500} 
			}
			
		},
		
		add: function(object, params){
			object = this.wrap(object, params.name);
			object.css({
				position: 'absolute',
				top: params.y, left: params.x,
				width: params.width,
				height: params.height,
				//overflow: 'auto',
				//'display': 'none',
				//'background-color': 'gray'
			});
			
			var id = app.util.random(20);
			this.objects[id] = {dragging: false, down: false, comp: object, id: id};
			this.objects[id].comp.find('.app_window_header').
					mousedown(this.objects[id], $.proxy(this.mousedown, this));
			return object;
		},
		
		wrap: function(element, name){
			var window =  $('<div></div>');
			var header = $('<div class="app_window_header"></div>')
			header.append('<span>'+name+'</span>')
			window.append(header).append(element);
			window.css(this.window_default_css);
			
			//var resizer = $('<span><img src"" /></span>');
			//window.append(resizer);
			//resizer.mousedown($.proxy(this.resizer_mousedown, this));
			//resizer.mousemove($.proxy(this.resizer_mousemove, this));
			//return window;
		},
		
		resizer_mousedown: function(e){
			var target = $(e.currentTarget);
			
		},
		
		registerEvents: function(obj){
			
		},
		
		setPointer: function(target, set){
			if(set) target.css({'cursor': 'move'});
			else target.css({'cursor': 'default'});
		},
		
		show: function(){
			this.$el.fadeIn();
		},
		hide: function(){
			this.$el.FadeOut();
		},
		
		close: function(){
			throw 'Not implemented';
		},
		
		setPos: function(x, y, target){
			target.css({left: x, top: y});
		},
		
		opacity: function(value, target){
			target.animate({opacity: value}, 200);
		},
		
		mousedown: function(e){
			var target = $(e.data.comp);

			this.setPointer(target, true);
			e.data.down = true;
			this.current = e.data;
			this.current.stickX = e.offsetX;
			this.current.stickY = e.offsetY;
			this.opacity('.4', this.current.comp);
		},
		mouseup: function(e){
			if(!this.current) return;
			 this.objects[this.current.id].down = false;
			//this.current.down = false;
			this.setPointer(this.current.comp, false);

			this.opacity('1', this.current.comp);
			this.current = null;
		},
		mouseout: function(e){
			this.down = false;
		},
		move: function(e){
			//console.log(this.down);
			if(this.current && this.current.down){
				var x = e.pageX - this.current.stickX;
				var y = e.pageY - this.current.stickY;
				
				var check = this.checkBounds(x, y, this.current.comp);
				//console.log(check, x, y);
				if(check.X) x = undefined;
				if(check.Y) y = undefined;
				
				this.setPos(x, y, this.current.comp);
			}
		},
		
		checkBounds: function(x, y, target){
			var stop = {}; stop.X = false; stopY= false;
			var width = target.width();
			var height = target.height();
			/*
			if(x <= this.bounds.topL.x || x+width >= this.bounds.bottomR.x)
				stop.X = true;
			if(y <= this.bounds.topL.y || y+height >= this.bounds.bottomR.y)
				stop.Y = true;
			
			return stop;
		}
	
	});
	*/
	
	DockWindow = {
		_initialize: function(params){
			this.__dragHandle = (typeof(params.dragHandle)!='undefined') ? params.dragHandle : this.$el;
			this.__resizeHandle = (typeof(params.resizeHandle)!='undefined') ? params.resizeHandle : this.$el;
			
			this._window_default_css = {
				'position': 'absolute',
				//'background-color': 'white',
				'border-radius': '5px',
				//'border' : '1px black solid',
				'padding' : '5px',
				width: '200px',
				height: '200px'
			};
			
			this.$el.css(this._window_default_css);
			$('html').mouseup($.proxy(this._mouseup, this));
			$('html').mousemove($.proxy(this._move, this));
			$(this.__dragHandle).mousedown($.proxy(this._mousedown, this));
			$(this.__resizeHandle).mousedown($.proxy(this._resizeDown, this));
		},
		
		_resizeDown: function(e){
			if(e.which != 1) return;
			this.__resizeDown = true;
		},
		
		_resizeElement: function(e){
			var width = this.$el.width(),
				height = this.$el.height();
			var posX = this.$el.position().left; posY = this.$el.position().top;

			var mx = e.pageX, my = e.pageY;
			width = mx - posX;
			height = my - posY;
			//console.log('W', width);
			if(width < 100) width = 100;
			if(height < 100) height = 100;
			this.$el.width(width).height(height);
		},
		
		_mousedown: function(e){
			if(e.which != 1) return;
			this._setPointer(true);
			this._down = true;
			this._stickX = e.offsetX;
			this._stickY = e.offsetY;
			this._opacity('.4');
		},
		
		_move: function(e){
			if(this._down){
				var x = e.pageX - this._stickX;
				var y = e.pageY - this._stickY;
				
				var check = this._checkBounds(x, y);
				if(check.X) x = undefined;
				if(check.Y) y = undefined;
				
				this._setPos(x, y);
			}else if(this.__resizeDown){
				this._resizeElement(e);
			}
		},
		_mouseup: function(e){
			this._down = false;
			this.__resizeDown = false;
			this._setPointer(false);
			this._opacity('1');
		},
		_mouseout: function(e){
			this._down = false;
			this.__resizeDown = false;
		},
		
		_show: function(){
			this.$el.fadeIn();
		},
		_hide: function(){
			this.$el.FadeOut();
		},
		_opacity: function(value){
			this.$el.animate({opacity: value}, 200);
		},
		_setPointer: function(set){
			if(set) this.$el.css({'cursor': 'move'});
			else this.$el.css({'cursor': 'default'});
		},
		_setPos: function(x, y){
			this.$el.css({left: x, top: y});
		},
		_checkBounds: function(x, y){
			var stop = {}; stop.X = false; stopY= false;
			var width = this.$el.width();
			var height = this.$el.height();
			/*
			if(x <= this.bounds.topL.x || x+width >= this.bounds.bottomR.x)
				stop.X = true;
			if(y <= this.bounds.topL.y || y+height >= this.bounds.bottomR.y)
				stop.Y = true;
			*/
			return stop;
		}
	
	
	}
	return DockWindow;

});
