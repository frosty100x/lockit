define(['backbone', 'app', 'comp/DockWindow'], function(Backbone, app, DockWindow){
	TreeView = Backbone.Marionette.CompositeView.extend({
		template: '#file-template',
		tagName: 'ul',
		self: null,
		initialize: function(data){
			var self = this;
			this.model.isOpen = false;
			this.id = this.model.id;
			this.mainTemplate = _.template( $('#tree-template').html() );
			
			_.bindAll(this, 'onRender');
		},
		
		//This is here so that child-trees don't implement DockWindow
		mainViewInit: function(){
			this._initialize({});
			$(this.mainTemplate).find('.app_tree_main').wrap(this.$el);
			console.log( $(this.mainTemplate).find('.app_tree_main') );
		},
		
		onRender: function(){
			console.log(this.id == '0');
			if(this.id == '0') this.mainViewInit();
		},
		//render: function(){  },  //override this to support animation,
		
		openFile: function(e){
			//this.model.triggerFileOpen(this.model.id);
			//app.fs.fileContents(this.model.id);
			this.model.openfile(this.model.id);
		},
		
		loadDir: function(){
			if(!this.model.isOpen){
				var self = this;
				//var nodes = self.model.get("children");
				this.model.fetch().done(function(data){
					//console.log(self);
					//self.store.addFiles(data);
					var nodes = new TreeModelCollection(data);
					//self.collection = new TreeModelCollection(self.model.children);
					self.collection = nodes;
					self.render();
				});
	
				this.model.isOpen = true;
			}else{
				this.$el.find('li').siblings().hide();
				this.model.isOpen = false;
			}
		},
		
		action: function(e){
			e.stopPropagation();
			var type = this.model.get("type");
			if(type == 'dir'){
				this.loadDir();
			}else if(type == "file"){
				this.model.open();
			}
		},
		
		view: function(){
			this.render();
			return this.$el;
		},
		
		events: {
			'dblclick li' : 'action'
		  }
	}).extend(DockWindow);
	return TreeView;	
});
