String.prototype.insert = function(c, p){
	return this.substring(0,p) + c + this.substring(p, this.length);
}

String.prototype.remove = function(st, end){
	return this.substring(0, st) + this.substring(end, this.length);
}

exports.vars = function(query, name){
    	var vars = query.split('&');
    	for (var i = 0; i < vars.length; i++) {
      	var pair = vars[i].split('=');
      	if (decodeURIComponent(pair[0]) == name) {
      		return decodeURIComponent(pair[1]);
		}
	}
	return null;
}

exports.queryToObject = function(query){
	var vars = query.split("&");
	var obj = {};
	for(i=0; i < vars.length; i++){
		var pair =  vars[i].split('=');
		if(pair.length < 2) continue;
		obj[pair[0]] = pair[1];
	}
	return obj;
}

exports.random = function(len){
	var min = 97, max = 123;
	var str = '';
	for(n=0; n < len; n++){
		var index = Math.floor(Math.random() * (max - min)) + min;
		str += String.fromCharCode(index);
	}
	return str;
}

exports.escape = function(data, chars){
	var l = data.length;
	function isIn(c){
		for(i=0; i < chars.length; i++) if(c == chars[i]) return true;
		return false;
	}
	for(n=0; n < l; n++){
		var c = data[n];
		if(isIn(c)){
			data = data.insert('\\', n);
			n++; continue;
		}
	}
	return data;
}
main = {};
main.test = function(){
	return 'xyz';
}
