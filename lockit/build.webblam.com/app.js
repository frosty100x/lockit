define(['require', 'backbone', 'marionette','libs/less.min',  'ace/ace',], 
function(require, Backbone, Marionette, less, ace){
	var app = new Backbone.Marionette.Application();

	_.mixin({
		url: function(str){
			var s=''; f=true;
			_.each(str, function(v, k){ s += ((f) ? '': '&') + k +'='+v; f=false; });
			return "?" + s;
		},
		merge: function(o1, o2){
			_.each(o2, function(v, k){ o1[k] = v; }); return o1;
		}
	});
	
	app.addRegions({
		main: ".app_main",
		header: ".app_header"
	});
	
	app.addInitializer(function(options){
		//var sid = $.cookie('_session');
		var sid = "kjghodgheghjlk";
		//console.log("Session: " + sid );
		if(typeof(sid) == 'undefined' || sid == ''){
			require(['comp/m_login', 'comp/v_login'], function(LoginModel, LoginView){
				app.header.show( new LoginView({
					model: new LoginModel()
				}));
			});
		}else{
			require(['comp/DockWindow', 'comp/workspace'], 
				function(DockWindow, Workspace){
					var ws  = new Workspace();
					
					//var win = new DockWindow();
					//var win2 = new DockWindow({x: 50, y: 50, width: 120, height: 155});
					//win.add( treeView.$el, {x: 10, y: 10, width: 100, height: 100} );
					
					//app.tree.show(treeView);
					app.main.show(ws);			
			});
		}
	});

	return app;
})

