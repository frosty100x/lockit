define(['backbone', 'marionette', 'app.js'], function(Backbone, Marionette, app){
	if(typeof(app) == 'undefined') return;
	app.module("fs", function(module, app, Backbone, Marionette, $, _) {
		var self = this;
		var root = 'dir';
		
		var t_fileContent = {type: 'file:content'},
			t_dirList = 		{type: 'dir:list'},
			t_fileRow = 	{type: "file:row"};
			t_fileObject= 	{type: 'file:obj'};
		var t_saveFile = 'file:save';
			
		this.fileContent = function(_id, cb){
			var obj = {id: _id};
			request(obj, t_fileContent, 'file:content:received', [_id], cb);
		}
		
		this.fileRow = function(_id, cb){
			var obj = {id: _id};
			var query = _.url(_.merge(obj, t_fileRow));
		
			$.get(url + root + query).done(function(data){
				var d = JSON.parse(data);
				app.trigger("file:row:received", d);
				if(_.isFunction(cb)) cb(_id, d);
			});
		}
		
		this.dirList = function(_id, cb){
			var obj = {id: _id};
			var query = _.url(_.merge(obj, t_dirList));
		
			$.get(url + root + query).done(function(data){
				var rows = JSON.parse(data);
				app.trigger("dir:received", _id, rows);
				if(_.isFunction(cb)) cb(_id, rows);
			});
		}
		
		this.fileObject = function(id, cb){
			var obj = {id: id};
			var query = _.url(_.merge(obj, t_fileObject));
			
			$.get(url + root + query).done(function(data){
				var len = Number.parseInt(data.substring(0, 5));
				var row = data.substr(5, len);
				var data = data.substring(5+row.length, data.length);
				try{ row = JSON.parse(row); }
				catch(e){
					console.error(e); return;
				}
				app.trigger('file:row:received', row);
				app.trigger('file:content:received', data, id);
				if(_.isFunction(cb)) cb(data, row);
			});
		
		}
		
		this.saveFile = function(_id, content, cb){
			var obj = {id: _id, type: t_saveFile};
			var query = _.url(obj);
			
			$.post(url + root + query, {content: content},
					function(data, status, xhr){
						if(_.isFunction(cb)) cb(data);
			 });
			/*
			$.ajax({type: 'POST', url: url + root + query, data: content, contentType: 'multipart/form-data'})
					.done(function(data, status, xhr){
						if(_.isFunction(cb)) cb(data);
					});
			*/
			//$.post(url + root + query, {content: content}, function(data){
				
			//});
		}
		
		function request(obj, base, event, eventargs, cb){
			var query = _.url(_.merge(obj, base));
			$.get(url + root + query).done(function(data){
				app.trigger.apply(app, [event, data].concat(eventargs));
				if(_.isFunction(cb)) cb.apply(app, [data].concat(eventargs));
			});
		}
	});
	return app.fs
});
