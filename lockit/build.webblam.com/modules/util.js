define(['backbone', 'app.js'], function(Backbone, app){
	app.module('util', function(module, app, Backbone, Marionette, $, _) {
		this.random = function(len){
			var st = 97, end = 122, str = '';
			for(i=0;i<len;i++){
				var rand = Math.floor(Math.random()*(end-st+1)+st)
				str += String.fromCharCode(rand);
			}
			return str;
		}
	});
	return app.util;
});
