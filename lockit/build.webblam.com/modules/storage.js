define(['app.js'], function(app){
	if(typeof(app) == 'undefined') return;
	app.module("store", function(module, app, Backbone, Marionette, $, _) {
		var self = this;
		var filePrefix = "file_row_";
		var fileContentPrefix = "file_content_"
		
		app.on('file:content:received', function(data, id){
			self.addFileContent(id, data);
		});
		
		app.on('file:row:received', function(row){
			self.addFile(row);
		});
		
		app.on('dir:received', function(rows){
			self.addFiles(rows);
		});
	
		//module.addInitializer(function(){});
		
		this.fileRow = function(id){
			return JSON.parse(localStorage.getItem(filePrefix + id));
		}
		
		this.fileContent = function(id){
			return localStorage.getItem(fileContentPrefix + id);
		}
		
		this.fileObject = function(id){
			var row = this.fileRow(id);
			var content = this.fileContent(id);
			if(row == null || content == null)
				return null
			//return [content, row];
			return {content: content, row: row};
		}
		
		this.addFileContent = function(id, data){
			localStorage.setItem(fileContentPrefix + id, data);
		}
		
		this.addFiles = function(items){
			if(!Array.isArray(items)){
				console.warn('Rejected non-object into store.addFiles()');
				return false;
			}
			_.each(items, self.addFile);
			return true;
		}
		
		this.addFile = function(item){
			if(!_.isObject(item) || _.isArray(item)){
				console.warn("Rejected non-object into store.addFile()");
				return false;
			}
			localStorage.setItem(filePrefix + item.id, JSON.stringify(item));
			return true;
		}
		
		this.clearAll = function(){
			localStorage.clear();
		}
		
	});
	return app.store;
});
