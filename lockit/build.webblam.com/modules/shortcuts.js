define(['app.js'], function(app){
	if(typeof(app) == 'undefined') return;
	app.module("shortcuts", function(module, app, Backbone, Marionette, $, _) {
		var self = this;
		var ctrl = false;
		
		var _shortcuts = [
			{name: 'ctrl+s', event: 'action:file:save'}
		];
		
		this.exists = function(str){
			return _.find(_shortcuts, function(item){ return item.name == str; });
		}
		
		this.keypress = function(e){
			var str = '';
			if(e.ctrlKey) str += 'ctrl+';
			if(e.altKey) str += 'alt+';
			str = (str += String.fromCharCode( e.keyCode )).toLowerCase();
			self.trigger('shortcut', str);
			var item = self.exists(str);
			if(!_.isUndefined(item)){
				console.warn("Preventing Default:", item.name);
				e.preventDefault();
				app.trigger(item.event);
			}
		}
		/*
		this.events = {
			'shortcut': 'fire'
		}
		*/
		//this.on('shortcut', this.fire, this);
		$('html').on('keydown', this.keypress);
	});
});
