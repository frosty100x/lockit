package lockit

import "labix.org/v2/mgo";
import "strings"

type Site struct{
	mongo * mgo.Session
	col * mgo.Collection
	Id string 			`bson:"_id,omitempty"`
	Url string 			`bson:"url,omitempty"`
	Name string 		`bson:"name,omitempty"`
	Host string 		`bson:"host,omitempty"`
	Active bool 		`bson:"active,omitempty"`
	Root string 		`bson:"root,omitempty"`
	Uid string 			`bson:"uid,omitempty"`
	Defaults []string 	`bson:"def,omitempty"`
	
}

func NewSite(m * mgo.Session) * Site{
	col := m.DB("users").C("sites");
	return &Site{mongo: m, col: col};
}

func (s * Site) Split() {
	s.Url = strings.Split(s.Url, ":")[0];
}

func (s * Site) Pull() bool{
	err := s.col.Find(s).One(&s);
	if err != nil{
		return false;
	}else{
		return true;
	}
}
