package lockit

import ("os"; "labix.org/v2/mgo";)

type ConfigBlock struct{
	Uid string
	DataRoot string
	TempDir string
	Port string
	
	FSObject bool
	HTTPObject bool
	DBObject bool
	DSObject bool
	
	Log bool
	LogFile string
	LogFileH * os.File
	
	Behind bool
	RealMode bool
	RealModeDir string
	RealModeConfigFile string
	Debug bool
	
	Mongo * mgo.Session
	FileSystem * mgo.Collection
	
	GlobalFileCache  map[string]*FileCache
	
	Random * RandomData
}
var Config ConfigBlock = ConfigBlock{}
