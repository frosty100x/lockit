package lockit

import "labix.org/v2/mgo"
import "net/http"
import "github.com/idada/v8.go"
import ("C"; "runtime"; "strconv"; "fmt";)
//import "fmt"
type ssmap map[string]string;

type Script struct{
	engine * v8.Engine
	context * v8.Context
	script * v8.Script;
	global * v8.ObjectTemplate;
	ex *v8.Value
	Mongo * mgo.Session
	Res http.ResponseWriter
	Req * http.Request
	
	uploads * Array
	KeyValue ssmap
	
	Uid string
	hostid string
	scriptFile * File
	globalFunc map[string]*v8.FunctionTemplate;
	cleanUp *Array
}

func NewScript(res http.ResponseWriter, req * http.Request, file * File, uploads * Array, keyvalue ssmap, engine * v8.Engine) *Script{
	s := &Script{Mongo:  Config.Mongo, Uid: Config.Uid, Res: res, Req: req, scriptFile: file, uploads: uploads, KeyValue: keyvalue, engine: engine}
	s.Init(engine);
	return s
}

func (e * Script) getEnv_url() * v8.Value {
	if e.Req == nil { return nil; }
	url := e.engine.NewObjectTemplate();
	url.SetProperty("host", e.engine.NewString(e.Req.Host), v8.PA_None);
	url.SetProperty("path", e.engine.NewString(e.Req.URL.Path), v8.PA_None);
	url.SetProperty("scheme", e.engine.NewString(e.Req.URL.Scheme), v8.PA_None);
	url.SetProperty("query", e.engine.NewString(e.Req.URL.RawQuery), v8.PA_None);
	v := e.engine.NewInstanceOf(url);
	url.Dispose();
	url = nil;
	return v
}

func (e * Script) getEnv_headers() * v8.Value{
	en := e.engine;
	headers := en.NewObjectTemplate();
	for key, value := range e.Req.Header {
		headers.SetProperty(key, e.engine.NewString(value[0]), v8.PA_None);
	}
	//e.cleanUp.Push(headers);
	v := en.NewInstanceOf(headers);
	headers.Dispose();
	headers = nil;
	en = nil
	return v;
}

func (e * Script) getEnv() * v8.Value {
	en := e.engine;
	env := en.NewObjectTemplate();
	env.SetProperty("url", e.getEnv_url(), v8.PA_None);
	env.SetProperty("headers", e.getEnv_headers(), v8.PA_None);
	//e.cleanUp.Push(env);
	v := en.NewInstanceOf(env);
	env.Dispose()
	env = nil;
	en = nil;
	return v
}

func (e * Script) jsFunc(name string, cb v8.FunctionCallback){
	e.globalFunc[name] = e.engine.NewFunctionTemplate(cb, nil);	
	e.global.SetAccessor(name, e.globalAccessor, nil, nil, v8.PA_None);  
}

func (e * Script) Init(engine * v8.Engine){
	
	if engine == nil {
		e.engine = v8.NewEngine();
	}else{
		e.engine = engine;
	}
	en :=  e.engine;
	e.globalFunc = make( map[string]*v8.FunctionTemplate, 5 );
	e.global = en.NewObjectTemplate();
	
	//e.global.SetProperty("env", en.NewInstanceOf(e.getEnv()), v8.PA_None);
	
	e.jsFunc("write", e.js_write);
	e.jsFunc("require", e.js_require);
	e.jsFunc("exit", e.js_exit);
	e.jsFunc("print", e.js_print);

	e.context = en.NewContext(e.global);
	en.AddMessageListener(e.msg);
	en = nil;
}

func (e * Script) msg(msg * v8.Message){
	line := strconv.Itoa(msg.Line);
	
	println(msg.Message + "\n" + msg.SourceLine);
	e.Res.Write([]byte(msg.Message+ "\n<br />"+ msg.SourceLine));
	
	print("at line ", line);
	e.Res.Write([]byte(" at line " + line));
}

func (e * Script) globalAccessor(name string, info v8.AccessorCallbackInfo) {
	info.ReturnValue().Set(e.globalFunc[name].NewFunction());
}

func (e * Script) js_write(info v8.FunctionCallbackInfo){
	for i := 0; i < info.Length(); i++ {
		e.Res.Write([]byte(info.Get(i).ToString()));
	}
	info.ReturnValue().Set(e.engine.True())
}

func (e * Script) Context() *v8.Context{
	return e.context;
}

func (e * Script) getExport(path string, cs * v8.ContextScope, shims * v8.Value) * v8.Value{
	if path == ""{
		println("Null path not alowed.");
		return nil;
	}
	
	tra := NewTraverser2(e.Mongo, path, e.scriptFile.Parent);
	var file * File = tra.Next2();
	
	for file != nil {
		if file.Type == "file" {
			scr := e.CompileJS(file.Content());
			cs.Run(e.CompileJS([]byte("exports = {}")));
			cs.Run(scr);

			if shims.IsUndefined() || shims.IsNull() {
				return cs.Global().GetProperty("exports");
			}
			
			//prop := cs.Global().GetPropertyNames();
			//fmt.Println(prop);
			
			if shims.IsString() {
				exports := shims.String();
				return cs.Global().GetProperty(exports);
			}
			
			if shims.IsArray() {
				shims := shims.ToArray();
				returnObj := e.engine.NewObjectTemplate();
				
				shimsLen := shims.Length();
				
				for i := 0; i < shimsLen; i++ {
					el := shims.GetElement(i);
					if !el.IsString() { continue; }
					elem := el.String();
					returnObj.SetProperty(elem, cs.Global().GetProperty(elem), v8.PA_None);
				}
				return e.engine.NewInstanceOf(returnObj);
			}
			return e.engine.Undefined();
		}
		file = tra.Next2();
	}
	return e.engine.Undefined();
}

func (e * Script) js_require(info v8.FunctionCallbackInfo){
	en := e.engine;
	str := info.Get(0).String();
	shims := info.Get(1);
	var obj *v8.ObjectTemplate;
	
	if str == "http" && Config.HTTPObject {
		obj = InstanceHTTP(e.engine);
	}else if str == "fs" && Config.FSObject {
		obj = InstanceFS(e.engine, e.Mongo, e.scriptFile, e.uploads, e.KeyValue);
	}else if str == "db" && Config.DBObject {
		obj = InstanceDB(e.engine, e.Mongo, e.Uid);
	}else if str == "store" && Config.DSObject {
		obj = InstanceDS(e.engine);
	}else{
		e.engine.NewContext(e.global).Scope(func(cs v8.ContextScope){
			//cs.Eval("exports = {} ");
			var export * v8.Value;
			//var text []byte;
			if Config.RealMode {
				//path := Config.RealModeDir + str;
				//cont, err := ioutil.ReadFile(path);
				//if err != nil { println(err); os.Exit(0); }
				//text = cont;
				
			}else{
				export = e.getExport(str, &cs, shims);
			}
			//cs.Run(e.CompileJS(text));
			info.ReturnValue().Set( export );
		});
		return;
	}

	//e.cleanUp.Push(obj);
	info.ReturnValue().Set( en.NewInstanceOf(obj) );
	en = nil
}

func(e * Script) js_exit(info v8.FunctionCallbackInfo){
	defer runtime.Goexit();
	info.ReturnValue().Set( e.engine.True() );
	e.Req.Body.Close();
}

func (e * Script) js_print(info v8.FunctionCallbackInfo){
	l := info.Length();
	for i := 0; i < l; i++ {
		print(info.Get(i).String());
	}
}

func (e * Script) Run(content string, getGlobal bool) string {
	/*
	defer func() {
		if r := recover(); r != nil {
			println("Script error", r)
			e.Res.Write([]byte("script error.\n"));
		}
	}()*/

	e.script = e.engine.Compile([]byte(content), nil)
	var data string;
	
	e.context.Scope(func(cs v8.ContextScope) {
		/*	defer func() {
			if r := recover(); r != nil {
				println("Script Recover:", r);
			}
		}()
		*/
		if e.Req != nil {
			cs.Global().SetProperty("env", e.getEnv());
		}
		result := cs.Run(e.script);
		data = result.String();
	});
	//e.global.Dispose();
	//e.engine.Dispose();
	//e.global = nil;
	//e.context = nil;
	
	//e.engine = nil;
	//e.CleanUp();
	//e.Res.Write([]byte("testing 1 2 3..."));
	return data;
}

func (e * Script) CleanUp(){
	v := e.cleanUp.Pop();
	
	for v != nil {
		v.(*v8.ObjectTemplate).Dispose();
		v = nil;
		v = e.cleanUp.Pop();
	}
}

func (e * Script) CompileJS(content []byte) *v8.Script {
	return e.engine.Compile(content, nil);
}

func (e * Script) RunScript(s * v8.Script) string{
	defer func() {
		if r := recover(); r != nil {
			if e.Res != nil { e.Res.Write([]byte("<div><b>error.</div>")); }
			fmt.Println(r);
		}
	}()

	var data string;
	e.context.Scope(func(cs v8.ContextScope) {
		if e.Req != nil {
			cs.Global().SetProperty("env", e.getEnv());
		}
		result := cs.Run(s);
		if result != e.engine.Null() || result != e.engine.Undefined() {
			data = result.String();
		}else{
			println("Null or Undefined Value.");
		}
	});
	return data;
}

func (e * Script) SetReqAndRes(req * http.Request, res * http.ResponseWriter){	
	e.Req = req; e.Res = *res;
}

func (e * Script) Engine() * v8.Engine {
	return e.engine;
}

func (e * Script) SetHostId(id string){
	e.hostid = id;
}
