package lockit

import "github.com/idada/v8.go"
import "net/http"
import "io/ioutil"
//import "fmt"
//import "reflect"

type HTTP struct{
	engine *v8.Engine
	response * http.Response
	funcTemp map[string]*v8.FunctionTemplate
}

func (e * HTTP) Init(en * v8.Engine){
	e.engine = en;
	e.funcTemp = make(map[string]*v8.FunctionTemplate, 5);
	
	e.funcTemp["get"] = en.NewFunctionTemplate(e.get, nil);
}

func (e *  HTTP) _get(url string) (string, bool) {
	res, err := http.Get(url);
	defer res.Body.Close()
	if err != nil {
		return "", true
	}
	body, _ := ioutil.ReadAll(res.Body);
	e.response = res;
	return string(body), false;
}

func (e * HTTP) accessor(name string, info v8.AccessorCallbackInfo){
	info.ReturnValue().Set( e.funcTemp[name].NewFunction() );
}

func (e * HTTP) BaseObject() *v8.ObjectTemplate {
	obj := e.engine.NewObjectTemplate();
	obj.SetAccessor("get", e.accessor, nil, nil, v8.PA_None);
	return obj;
}

func (e * HTTP) get(info v8.FunctionCallbackInfo){
	en := e.engine;
	rObj := en.NewObjectTemplate();
	headers := en.NewObjectTemplate();
	var urlStr string = info.Get(0).String();
	var isFirst bool = true;
	params := info.Get(1);
	if params.IsObject() {
		if urlStr[len(urlStr)-1] != '?' {
			urlStr += "?";
		}
		obj := params.ToObject();
		params := obj.GetPropertyNames();
		
		for n := 0;  n < params.Length(); n++ {
			key := params.GetElement(n).ToString();
			value := obj.GetProperty(key)
			if !value.IsObject() {
				if !isFirst {
					urlStr += "&";
				}
				isFirst = false;
				urlStr += key + "=" + value.ToString();
			}
		}
	}
	data, err := e._get( urlStr );
	if err {
		info.ReturnValue().Set( en.Null() );
		return;
	}
	
	for key, value := range e.response.Header {
		headers.SetProperty(key, en.NewString(value[0]), v8.PA_None);
	}
	
	rObj.SetProperty("status", en.NewInteger(int64(e.response.StatusCode)), v8.PA_None);
	rObj.SetProperty("Content-Length", en.NewInteger(int64(e.response.ContentLength)), v8.PA_None);
	
	rObj.SetProperty("body", en.NewString(data), v8.PA_None);
	rObj.SetProperty("headers", en.NewInstanceOf(headers), v8.PA_None);
	
	info.ReturnValue().Set( en.NewInstanceOf(rObj) );
}

func InstanceHTTP(en * v8.Engine) *v8.ObjectTemplate {
	obj := &HTTP{};
	obj.Init(en);
	return obj.BaseObject();
}
