package lockit

type DataCallback func(text []byte, _type string, evaltype string);

type Parser struct{
	cb DataCallback
	buffer []byte;
	bufferSize int
	chunker * Chunker
}

func (e * Parser) Init(cb DataCallback, c * Chunker){
	e.cb = cb;
	e.buffer = make([]byte, 500);
	e.bufferSize = len(e.buffer);
	e.chunker = c;
	e.parse();
}

func (e * Parser) readQuote(q byte) string {
	var b byte = 1; var str string = string(q);
	for b != 0 {
		br := b;
		b = e.chunker.Next()
		if b == q && br != 92 {
			return str;
		}
		str += string(b);
	}
	return str;
}

func (e * Parser) parse(){
	var b byte = 1;
	for b != 0 {
		b = e.chunker.Next()
		b2 := e.chunker.Peek()
		if b == 60 && b2 == 37 {		// if b == < && b2 == %
			e.chunker.Next();
			p := e.chunker.Next();
			rr := []byte(e.readJS());
			if p == 61 {					// if p = "="
				e.cb(rr, "js", "equal");
			}else if p == 45 {			// if p == '-'
				e.cb(rr, "js", "dash");
			}else{
				e.cb(rr, "js", "none");
			}
			continue;
		}else{
			e.cb([]byte(string(b)), "html", "");
		}
	}
}

func (e  * Parser) readJS() string{
	var b byte = 1; var js string="";
	for b != 0 {
		b = e.chunker.Next()
		b2 := e.chunker.Peek()
		
		if b == 34 || b == 39 { 		// if b == " || b == '
			js += e.readQuote(b);
		}
		if b == 37 && b2 == 62 { 	//if b == % && b2 == >
			e.chunker.Next();
			return js;
		}
		js += string(b);
	}
	return js;
}

func NewParser(cb DataCallback, c * Chunker) * Parser{
	p := &Parser{};
	p.Init(cb, c);
	return p
}
