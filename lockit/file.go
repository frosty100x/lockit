package lockit 

import "labix.org/v2/mgo"
import ("io/ioutil"; "os"; "io"; "fmt"; "time"; "errors"; "strings";)
import "labix.org/v2/mgo/bson"

type File struct{
	mongo * mgo.Session
	col * mgo.Collection
	Id string		`bson:"_id,omitempty"`
	Uid string		`bson:"uid,omitempty"`
	Name interface{}	`bson:"name,omitempty"`
	Host string		`bson:"host,omitempty"`
	Type string		`bson:"ty,omitempty"`
	Parent string	`bson:"parent,omitempty"`
	Ext string		`bson:"ext,omitempty"`
	//Size int			`bson:"size,omitempty"`
	ReturnType string `bson:"rt,omitempty"`
	Default string 	`bson:"def,omitempty"`
	Root bool		`bson:"r,omitempty"`
	ModifiedDate int	`bson:"md,omitempty"`
}

type FileSystemStruct struct{
	col * mgo.Collection
}

func (fs * FileSystemStruct) Find(f * File) * File{
	fs.col = Config.FileSystem;
	result := &File{};
	err := fs.col.Find(f).One(&result);
	if err == nil {
		return result
	}else{
		return nil
	}
}

func (e * FileSystemStruct) Iter(f * File) *mgo.Iter {
	e.col = Config.FileSystem;
	iter := e.col.Find(f).Iter();
	return iter;
}

func (fs * FileSystemStruct) Content(f * File) []byte {
	util := NewUtil();
	path := util.ToPath(f.Host, f.Id)
	buff, err := ioutil.ReadFile(path)
	if err != nil{
		fmt.Println(err);
		return []byte("");
	}
	return buff;
}

func (e * FileSystemStruct) UpdateModTime(f * File) bool{
	time := int(time.Now().UnixNano() / int64(time.Millisecond))
	change := bson.M{"$set" : bson.M{"md" : time }};
	err := e.col.Update(&f, change);
	if  err != nil {
		return false;
	}
	f.ModifiedDate = time;
	return true;
}

func (e * FileSystemStruct) Remove(f * File, reinsertOnFail bool) error {
	err := e.col.Remove(f);
	util := NewUtil();
	if err == nil{
		if f.Type == "dir" {
			return nil;
		}
		path := util.ToPath(f.Host, f.Id);
		
		if _, err = os.Stat(path); err != nil || path == "" {
			return errors.New("Not Found on Disk: " + f.Name.(string));
		}
		err2 := os.Remove(path);
		if err2 != nil{
			if reinsertOnFail {
				//fmt.Println("Failed: Reinserting doc:", f);
				e.col.Insert(f);
			}
			return err2;
		}
		return nil;
	}else{
		return err;
	}
}

func (fs * FileSystemStruct) Create(file * File, reader io.Reader) error {
	fs.col = Config.FileSystem;
	util := NewUtil();
	err := fs.col.Insert(file);
	rand := NewRandomData();
	var retry int = 0;
	
	for err != nil {	
		fmt.Println(err);
		//e.log("error inserting doc", file.Name);
		if strings.Contains(err.Error(), "11000 E11000") && retry < 3 {
			retry++;
			file.Id = rand.Random(20);
			err = fs.col.Insert(file);
			continue;
		}else{
			return errors.New("Error inserting doc.");
		}	
	}
	
	if file.Type == "file" {
		path := util.ToDir(file.Host, file.Id);
		if _, created := os.Stat(path); created != nil {
			err := os.Mkdir(path, os.ModeDir );
			if err != nil {
				return errors.New("Failed to create directory: " + path);
			}
		}

		f, err := os.Create(path + file.Id)
		if err == nil {
			if reader != nil{
				_, err := io.Copy(f, reader);
				if err != nil {
					return errors.New("Failed to copy reader to file: " + file.Name.(string)); 
				}
				return nil;
			}
		}else{
			return errors.New("Failed creating disk file: " + path + file.Id);
		}
	}
	return nil;
}

func NewFile() * File{
	f := &File{}
	f.Init();
	return f;
}

func (e * File) Init(){
	e.col = Config.FileSystem;
}

func (e * File) Get() * File{
	f := NewFile();
	err := e.col.Find(e).One(&f);
	if err != nil{
		return nil;
	}else {
		return f;
	}
}

func (e * File) Pull() bool {
	err := e.col.Find(e).One(&e);
	if err != nil{
		return false;
	}else {
		return true;
	}
}

func (e * File) Iter() *mgo.Iter {
	//var result interface{} = 0;
	iter := e.col.Find(e).Iter();
	return iter;
}

func (e * File) Content() []byte {
	util := NewUtil();
	path := util.ToPath(e.Host, e.Id)
	buff, err := ioutil.ReadFile(path)
	if err != nil{
		fmt.Println(err);
		return []byte("");
	}
	return buff;
}

func (e * File) log(obj... interface{}){
	if Config.Debug {
		for _, s := range obj {
			fmt.Print(s);	
		}
	}
}

func (e * File) Create(file io.Reader) error{
	util := NewUtil();
	err := e.col.Insert(e);
	
	if err != nil{
		e.log("error inserting doc", e);
		return errors.New("Error inserting doc.");
	}else{
		if e.Type == "file" {
			path := util.ToDir(e.Host, e.Id);
			err = os.Mkdir(path, os.ModeDir );
			if err != nil {
				e.log(err);
				return errors.New("Could not create directory.");
			}
			f, err := os.Create(path + e.Id)
			if err == nil {
				if file != nil{
					_, err := io.Copy(f, file);
					if err != nil {
						e.log(err);
						return errors.New("Error copying reader -> file. " + e.Name.(string)); 
					}
					return nil;
				}
			}else{
				e.log(err);
				return errors.New("Error: creating " + path + e.Id);
			}
		}
		return nil;
	}
}

func (e * File) ToString() string{
	return fmt.Sprint("Id ", e.Id, " - Name ", e.Name, " - Uid ", e.Uid, " - Host ", e.Host, " - Type ", e.Type, 
			" - Parent ", e.Parent, " - Ext ", e.Ext, " - ReturnType ", e.ReturnType, " - Default ", e.Default);
}

func (e * File) Remove() bool {
	err := e.col.Remove(e);
	util := NewUtil();
	if err == nil{
		if e.Type == "dir" {
			return false;
		}
		path := util.ToPath(e.Host, e.Id);
		err2 := os.Remove(path);
		if err2 != nil{
			return true;
		}
	}
	return false;
}

var FS * FileSystemStruct = &FileSystemStruct{col: Config.FileSystem};
