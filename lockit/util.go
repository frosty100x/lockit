package lockit

import (
	"math/rand"
	"syscall"
);

type Util struct{
	Root string
	TempDir string
}

type RandomData struct {
	src rand.Source
	rand * rand.Rand
}

func NewUtil() Util{
	ut := Util{Root: Config.DataRoot, TempDir: Config.TempDir};
	return ut;
}

func (e Util) ToPath(hid string, fid string) string {
	if len(hid) < 2 || len(fid) < 2 { return ""; }
	return e.Root + "/" + hid[0:2] + "/" + hid + "/"+  fid[0:2] + "/" + fid;
}

func (e Util) ToDir(hid, fid string) string {
	return e.Root + "/" + hid[0:2] + "/" + hid + "/"+  fid[0:2] + "/";
}

func (e Util) ToTemps(hid string) string{
	return e.TempDir + "/" + hid + "/";
}

func (e Util) dbFields(length int) []string {
	st := 97; en := 122; pos := st;
	array := make([]string, 0, 26);
	for n := 0; n < length; n++ {
		array = array[:len(array)+1];
		array[n] = string(pos);
		if pos > en {
			pos = st;	
		}
		pos++;
	}
	return array;
}

func NewRandomData() * RandomData{
	r := &RandomData{};
	r.Init();
	return r;
}

func (r * RandomData) Init(){
	var tv syscall.Timeval;
	syscall.Gettimeofday(&tv)
	r.src = rand.NewSource(int64(tv.Sec));
	r.rand = rand.New(r.src);
}

func (r * RandomData) Random(length int) string{
	var str string;
	st := 97; en := 122;
	
	for n := 0;n<length;n++ {
		i := (r.rand.Int() % (en - st+ 1)) + st;
		str += string(i);
	}
	return str;
}

type Array struct{
	data []interface{}
	growRate int
	length int
}

func NewArray() * Array{
	arr := &Array{};
	arr.Init();
	return arr;
}

func (a * Array) GrowRate(r int){
	a.growRate = r;
}

func (a * Array) Init(){
	a.length = 0;
	a.growRate = 10;
	a.data = make([]interface{}, 0, a.growRate);
}

func (a * Array) Push(obj interface{}){
	l := len(a.data);
	c := cap(a.data);
	
	if l+1 > c{
		tmp := make([]interface{}, l, c+a.growRate);
		copy(tmp, a.data);
		a.data = tmp;
	}
	a.data = a.data[:l+1];
	a.data[l] = obj;
	a.length++;
}

func (a * Array) Pop() interface{} {
	if a.length > 0 {
		obj := a.data[a.length-1];
		a.data = a.data[:a.length-1];
		a.length--;
		return obj
	}
	return nil
}

func (a * Array) Length() int {
	return a.length;
}

func (a * Array) Data() []interface{}{
	return a.data;
}

func (a * Array) Contains(item interface{}) bool{
	for _, v := range a.data {
		if v == item { return true; }
	}
	return false;
}

func copyOffset(dest, src []byte, offset int) int {
	srcSize := len(src);
	destSize := len(dest);
	sr := 0;
	//println("Offset",offset, "DestSize", destSize, "SrcSize", srcSize);
	
	for i := offset; i < destSize && sr < srcSize; i++ {
		//println("Offset",offset, "I", i, "SR", sr);
		dest[i] = src[sr]; sr++;
		
	}
	return sr;
}
