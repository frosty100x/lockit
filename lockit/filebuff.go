package lockit 

import "bytes"
import "net/http"
import "bufio"
import "html"
import "fmt"
import "os"
import "github.com/idada/v8.go"

type Temp struct{
	res http.ResponseWriter
	reader bytes.Buffer

}

type FileBuff struct{
	fixed string
	fixedReader bufio.Reader
	writer *bytes.Buffer
	chunker * Chunker;
	lastPos int;
	name string; path string;

	flushBuff []byte
	scripts map[int]*v8.Script;
	scriptPos int
	scriptInserts []int
	out http.ResponseWriter
	script * Script
}

func (e * FileBuff) Init(script *Script, out http.ResponseWriter, path, name string){
	e.flushBuff = make([]byte, 1024);
	e.scripts = make(map[int]*v8.Script, 5);
	//scriptInserts = make([]int, 10);
	e.script = script;
	//e.fixedReader = bufio.NewReader( e.flushBuff );
	e.writer = bytes.NewBuffer(e.flushBuff);
	e.writer.Reset();
	e.chunker = e.newChunker(path);
	e.out = out;
	e.scriptPos = 0;
	e.lastPos = 0;
	e.name = name; e.path  = path;
	e.miscRun();
}

func (e * FileBuff) newChunker(path string) * Chunker{
	f, err := os.Open(path);
	if err != nil {
		e.out.Write([]byte("Could not open file:" + path));
		return nil;
	}
	reader := bufio.NewReader(f);
	tmp := &Chunker{};
	tmp.Init(reader, 1024);
	return tmp;
}

func (e * FileBuff) readQuote(q byte) string {
	var b byte = 1; var str string = string(q);
	for b != 0 {
		br := b;
		b = e.chunker.Next()
		if b == q && br != 92 {
			return str;
		}
		str += string(b);
	}
	return str;
}

func (e  * FileBuff) readJS() string{
	var b byte = 1; var js string="";
	for b != 0 {
		b = e.chunker.Next()
		b2 := e.chunker.Peek()
		
		if b == 34 || b == 39 { // if b == " || b == '
			js += e.readQuote(b);
		}
		if b == 37 && b2 == 62 { //if b == % && b2 == >
			e.chunker.Next();
			return js;
		}
		js += string(b);
	}
	return js;
}

func (e * FileBuff) miscRun(){
	var b byte = 1;
	cp := 0;
	for b != 0 {
		b = e.chunker.Next()
		b2 := e.chunker.Peek()
		//if b == 34 || b == 39 { // if b == " || b == '
			//rt := e.readQuote(b);
			//e.out.Write([]byte(rt));
			//e.writer.WriteString( rt );
		//}
		if b == 60 && b2 == 37 {					// if b == < && b2 == %
			
			e.chunker.Next();
			p := e.chunker.Next();
			//cp := e.chunker.Position();
			
			rr := e.readJS();
			fmt.Println(rr);
			
			tmps := e.script.CompileJS([]byte(rr));
			if p == 61 {						// if p = "="
				value := e.script.RunScript(tmps);
				e.out.Write([]byte( value ));
			}else if p == 45 {					// if p == '-'
				value := e.script.RunScript(tmps);
				data := html.EscapeString( value);
				e.out.Write([]byte(data));
			}else{
				e.script.Run( rr, false);
			}
			e.scripts[cp] = tmps;
			continue;
		}else{
			cp++;
		}
		e.out.Write([]byte(string(b)));
		e.writer.WriteByte(b);
	}
	//fmt.Println("Byte 1: ", e.writer.Bytes());
}

func (e * FileBuff) writeLoop(){
	buff := e.writer.Bytes();
	for i, scr := range e.scripts {
		e.writeUntil(i, buff);
		e.lastPos = i;
		value := e.script.RunScript(scr);
		e.out.Write([]byte(value));
	}
}

func (e * FileBuff) writeUntil(i int, buff []byte){
	println("Write until:", e.lastPos);
	for n := e.lastPos; n<i; n++ {
		println(n, "-", string(buff[n]));
		e.out.Write([]byte(string(buff[n])));
	}
}

func (e * FileBuff) Pipe(res http.ResponseWriter){
	//cc := Temp{Res: res};
	e.out = res;
	e.writeLoop();
}

func (e FileBuff) newReader() *bytes.Reader {
	return bytes.NewReader( e.flushBuff );
}

func (e FileBuff) Name() string {
	return e.name;	
}
