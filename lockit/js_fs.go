package lockit;

import "github.com/idada/v8.go"
import "labix.org/v2/mgo"

type FileSystem struct{
	Mongo * mgo.Session
	Col * mgo.Collection
	Uid string
	Script * v8.Script
	engine *v8.Engine
	Context *v8.Context
	fs * GoFileSystem
	//query map[string]interface{}
	//result [] map[string]interface{}
	//dbstring string
	pack string "pack1"
	scriptFile * File
	
	fileUploads * Array
	KeyValue map[string]string
	
	funcTemp map[string]*v8.FunctionTemplate
}

type FileHandle struct{
	engine * v8.Engine
	Uid string
	Fs * GoFileHandle
	funcTemp map[string]*v8.FunctionTemplate
}

func (f * FileSystem) Init(en * v8.Engine){
	f.engine = en;
	f.funcTemp = make(map[string]*v8.FunctionTemplate);
	f.funcTemp["delete"] = en.NewFunctionTemplate(f.delete, nil);
	f.funcTemp["open"] = en.NewFunctionTemplate(f.open, nil);
	f.funcTemp["openById"] = en.NewFunctionTemplate(f.openById, nil);
	f.funcTemp["list"] = en.NewFunctionTemplate(f.list, nil);
	f.funcTemp["listById"] = en.NewFunctionTemplate(f.listById, nil);
	f.funcTemp["fileById"] = en.NewFunctionTemplate(f.fileById, nil);
	
	f.funcTemp["uploads"] = en.NewFunctionTemplate(f.uploads, nil);
	f.funcTemp["commit"] = en.NewFunctionTemplate(f.commit, nil);
	f.funcTemp["create"] = en.NewFunctionTemplate(f.create, nil);
	f.funcTemp["keys"] = en.NewFunctionTemplate(f.keys, nil);
	f.funcTemp["key"] = en.NewFunctionTemplate(f.key, nil);
	f.fs = NewGoFileSystem(f.scriptFile, f.fileUploads, f.KeyValue);
}

func (f * FileSystem) jsFunc(name string, cb v8.FunctionCallback){
	f.funcTemp[name] = f.engine.NewFunctionTemplate(cb, nil);
}

func NewFileHandle(en * v8.Engine, handle * GoFileHandle, uid string) * FileHandle{
	fh := &FileHandle{engine: en, Fs: handle, Uid: uid};
	fh.Init();
	return fh;
}

func (f * FileHandle) Init(){
	en := f.engine;
	f.funcTemp = make(map[string]*v8.FunctionTemplate);
	f.funcTemp["read"] = en.NewFunctionTemplate(f.read, nil);
	f.funcTemp["write"] = en.NewFunctionTemplate(f.write, nil);
	f.funcTemp["zero"] = en.NewFunctionTemplate(f.write, nil);
}

func (f * FileHandle) Accessor(name string, info v8.AccessorCallbackInfo){
	info.ReturnValue().Set(f.funcTemp[name].NewFunction());
}

func (f * FileHandle) read(info v8.FunctionCallbackInfo){
	en := f.engine;
	str, n := f.Fs.Read();
	if n == 0 {
		info.ReturnValue().Set( en.Null() );
		return;
	}
	info.ReturnValue().Set( en.NewString(str));
}

func (f * FileSystem) null(info v8.FunctionCallbackInfo){
	info.ReturnValue().Set(f.engine.Null() );
}

func (f * FileHandle) write(info v8.FunctionCallbackInfo){
	en := f.engine;
	p1 := info.Get(0);
	
	if !p1.IsString() {
		info.ReturnValue().Set( en.False() );
		return;
	}
	str := p1.String();
	
	n := f.Fs.Write(str);
	println(n, " ", str);
	info.ReturnValue().Set( en.NewInteger(int64(n)));
}

func ( f * FileHandle) zero(info v8.FunctionCallbackInfo){
	f.Fs.Zero();
	info.ReturnValue().Set( f.engine.True() );
}

func (f * FileSystem) fileById(info v8.FunctionCallbackInfo){
	en := f.engine;
	
	p1 := info.Get(0);
	
	if !p1.IsString() {
		println("fileById() accepts string only");
		f.null(info);
		return;
	}
	file := f.fs.FileById(p1.String())
	if file == nil {
		f.null(info);
		return;
	}
	obj := en.NewObjectTemplate();
	
	obj.SetProperty("id", en.NewString(file.Id), v8.PA_None);
	obj.SetProperty("name", en.NewString(file.Name.(string)), v8.PA_None);
	obj.SetProperty("rt", en.NewString(file.ReturnType), v8.PA_None);
	obj.SetProperty("parent", en.NewString(file.Parent), v8.PA_None);
	obj.SetProperty("ty", en.NewString(file.Type), v8.PA_None);
	
	info.ReturnValue().Set( en.NewInstanceOf( obj ) );
}

func (f * FileSystem) open(info v8.FunctionCallbackInfo){
	en := f.engine;
	file := info.Get(0);
	mode := info.Get(1);
	size := info.Get(2);
	
	if !file.IsString() || !size.IsInt32() || !mode.IsString() {
		//f.Context.ThrowException("open excepts (string, int)")
		println("open excepts (file, mode, size)");
		return;
	}
	//println(file.String(), " ", size.ToInt32());
	handle := f.fs.Open(file.String(), mode.String(), int(size.ToInt32()));
	if handle == nil {
		info.ReturnValue().Set( en.Null() );
		return;
	}
	fh := NewFileHandle(en, handle, f.Uid);
	h := f.HandleObject(fh);
	info.ReturnValue().Set( en.NewInstanceOf(h) );
}

func (f * FileSystem) openById(info v8.FunctionCallbackInfo){
	en := f.engine;
	file := info.Get(0);
	mode := info.Get(1);
	size := info.Get(2);
	
	if !file.IsString() || !size.IsInt32() || !mode.IsString(){
		//f.Context.ThrowException("open excepts (string, int)")
		println("open excepts (file, mode, size)");
		return;
	}
	
	handle := f.fs.OpenById(file.String(), mode.String(), int(size.ToInt32()));
	if handle == nil {
		info.ReturnValue().Set( en.Null() );
		return;
	}
	fh := NewFileHandle(en, handle, f.Uid);
	h := f.HandleObject(fh);
	info.ReturnValue().Set( en.NewInstanceOf(h) );
}

func (f * FileSystem) delete(info v8.FunctionCallbackInfo){
	en := f.engine;
	str := info.Get(0);
	
	if !str.IsString(){
		info.ReturnValue().Set( en.False() );
		return;
	}
	
	if f.fs.Delete( str.String() ){
		info.ReturnValue().Set( en.True() );
	}else{
		info.ReturnValue().Set( en.False() );
	}
}

func (f * FileSystem) create(info v8.FunctionCallbackInfo){
	en := f.engine;
	str := info.Get(0);
	
	if !str.IsString(){
		info.ReturnValue().Set( en.False() );
		print("not a string");
		return;
	}
	
	if f.fs.Create( str.String(), nil, "") == nil {
		info.ReturnValue().Set( en.True() );
	}else{
		info.ReturnValue().Set( en.False() );
	}
}

func (f * FileSystem) commit(info v8.FunctionCallbackInfo){
	en := f.engine;
	str := info.Get(0);
	str2 := info.Get(1);
	str3 := info.Get(2);
	var id string;
	
	if !str.IsString() || !str2.IsString() || !str3.IsString() {
		println("not a string");
		return;
	}
	id = str.String();
	path := str2.String();
	rt := str3.String();
	if f.fs.Commit(id, path, rt){
		info.ReturnValue().Set( en.True() );
	}else{
		info.ReturnValue().Set( en.False() );
	}
}

func (f * FileSystem) key(info v8.FunctionCallbackInfo) {
	en := f.engine;
	str := info.Get(0);
	
	if !str.IsString(){
		println("key() accepts 1 string");
		info.ReturnValue().Set( en.False() );
		return;
	}
	value := f.fs.Key( str.String() );
	info.ReturnValue().Set( en.NewString( value));
}

func (f * FileSystem) keys(info v8.FunctionCallbackInfo){
	en := f.engine;
	n := 0;
	data := f.fs.Keys();
	arr := en.NewArray(len(data));
	
	for k, v := range data {
		obj := en.NewObjectTemplate();
		obj.SetProperty(k, en.NewString(v), v8.PA_None);
		arr.ToArray().SetElement(n, en.NewInstanceOf(obj));
		n++;
	}
	info.ReturnValue().Set( arr );
}

func (f * FileSystem) uploads(info v8.FunctionCallbackInfo){
	en := f.engine;
	files := f.fs.Uploads()
	l := len(files);
	
	arr := en.NewArray(l);
	for i, upload := range files {
		obj := en.NewObjectTemplate();
		
		for k, v := range upload {
			obj.SetProperty(k, en.NewString(v), v8.PA_None);
		}
		arr.ToArray().SetElement(i, en.NewInstanceOf(obj));
	}
	
	info.ReturnValue().Set( arr );
}

func (f * FileSystem) list(info v8.FunctionCallbackInfo){
	en := f.engine;
	var path string;
	if !info.Get(0).IsString() {
		info.ReturnValue().Set( en.Null() );
		return;
	}
	path = info.Get(0).String();
	err, arr := f.fs.List(path);
	
	jsArr := en.NewArray(arr.Length());
	if !err {
		result := arr.Pop();
		
		for n:=0; result != nil; n++ {
			file := result.(File);
			obj := en.NewObjectTemplate();
			obj.SetProperty("id", en.NewString(file.Id), v8.PA_None);
			obj.SetProperty("name", en.NewString(file.Name.(string)), v8.PA_None);
			obj.SetProperty("type", en.NewString(file.Type), v8.PA_None);
			
			jsArr.ToArray().SetElement(n, en.NewInstanceOf(obj));
			result = arr.Pop();
		}
		info.ReturnValue().Set(jsArr);
		return;
	}
	info.ReturnValue().Set(en.Null());
}

func (f * FileSystem) listById(info v8.FunctionCallbackInfo){
	en := f.engine;
	str := info.Get(0);
	if !str.IsString(){
		println("listById excepts 1 string");
		return;
	}
	id := str.String();
	if id == "" {
		println("id cannot be blank");
		return;
	}
	err, arr := f.fs.ListById(id);
	if err {
		println("error");	
		return;
	}
	jArr := en.NewArray(arr.Length());
	g := arr.Pop();
	for n := 0; g != nil; n++ {
		file := g.(File);
		obj := en.NewObjectTemplate();
		obj.SetProperty("id", en.NewString(file.Id), v8.PA_None);
		obj.SetProperty("name", en.NewString(file.Name.(string)), v8.PA_None);
		obj.SetProperty("type", en.NewString(file.Type), v8.PA_None);
		
		jArr.ToArray().SetElement(n, en.NewInstanceOf(obj));
		g = arr.Pop();
	}
	info.ReturnValue().Set( jArr );
}

func (f * FileSystem) Accessor(name string, info v8.AccessorCallbackInfo) {
	info.ReturnValue().Set(f.funcTemp[name].NewFunction());
}

func (f * FileSystem) HandleObject(handle * FileHandle) * v8.ObjectTemplate{
	en := f.engine;
	obj := en.NewObjectTemplate();
	obj.SetAccessor("read", handle.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("write", handle.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("zero", handle.Accessor, nil, nil, v8.PA_None);
	return obj;
}

func (f * FileSystem) BaseObject() * v8.ObjectTemplate{
	en := f.engine;
	obj := en.NewObjectTemplate()
	obj.SetAccessor("create", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("delete", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("open", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("openById", f.Accessor, nil, nil, v8.PA_None);
	
	obj.SetAccessor("fileById", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("list", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("listById", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("uploads", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("commit", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("key", f.Accessor, nil, nil, v8.PA_None);
	obj.SetAccessor("keys", f.Accessor, nil, nil, v8.PA_None);
	return obj;
}

func InstanceFS(e * v8.Engine, m * mgo.Session, script * File, uploads * Array, keyvalue map[string]string) *v8.ObjectTemplate {
	f := &FileSystem{Mongo: m, Uid: Config.Uid, scriptFile: script, fileUploads: uploads, KeyValue: keyvalue};
	f.Init(e);
	return f.BaseObject();
}
