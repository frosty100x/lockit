package lockit

import "github.com/idada/v8.go"
import "labix.org/v2/mgo"
import ("fmt");

type DataStore struct{
	mongo * mgo.Session
	funcTemp map[string]*v8.FunctionTemplate
	DS * GoDataStore
	engine * v8.Engine
}


func NewDataStore(e * v8.Engine) * DataStore {
	goDS := NewGoDataStore();
	ds := &DataStore{DS : goDS};
	ds.Init(e);
	return ds;
}

func (d * DataStore) Init(en * v8.Engine){
	d.engine = en; 
	d.funcTemp = make(map[string]*v8.FunctionTemplate);
	d.jsFunc("createURLStore", d.createUrlStore);
}

func (d * DataStore) createUrlStore(info v8.FunctionCallbackInfo){
	en := d.engine;
	path := info.Get(0);
	fid := info.Get(1);
	
	if !path.IsString() || !fid.IsString() {
		fmt.Println("createURLStore() excepts (string, string)");
		return 
	}
	strPath := path.String(); strFid := fid.String();
	err := d.DS.CreateURLStore(strPath, strFid);
	if err != nil {
		info.ReturnValue().Set(en.False());
	}else{
		fmt.Println(err);
		info.ReturnValue().Set(en.True());
	}
}


func (d * DataStore) jsFunc(name string, cb v8.FunctionCallback){
	d.funcTemp[name] = d.engine.NewFunctionTemplate(cb, nil);
}

func (d *  DataStore) Accessor(name string, info v8.AccessorCallbackInfo) {
	info.ReturnValue().Set(d.funcTemp[name].NewFunction());
}

func (d * DataStore) BaseObject() * v8.ObjectTemplate {
	en := d.engine;
	obj := en.NewObjectTemplate()
	obj.SetAccessor("createURLStore", d.Accessor, nil, nil, v8.PA_None);
	return obj;
}

func InstanceDS(e * v8.Engine) * v8.ObjectTemplate {
	ds := NewDataStore(e)
	return ds.BaseObject();
}
