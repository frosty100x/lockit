package lockit

import "strings"
import "labix.org/v2/mgo"
import "fmt"

type Pa2 struct{
	Name string
	Type string
	Back bool
}

type Traverser2 struct{
	mongo * mgo.Session
	col * mgo.Collection
	arPath []Pa2
	arLen int
	counter  int
	pos int
	lastAttempt * File
	Uid string
	Root string
	Path string
	queue []*File
	rootPulled bool
	abs bool
}

func NewTraverser2(m * mgo.Session, path, root  string) * Traverser2{
	t := &Traverser2{mongo: m, Uid: Config.Uid, Path: path, Root: root};
	t.Init();
	return t;
}

func (t * Traverser2) Init(){
	t.queue = make([]*File, 0, 10);
	t.counter = 0; t.pos = 0;
	t.rootPulled = false;
	t.abs = false;
	t.parse();
	//t.grabRoot();
}

func (t * Traverser2) SetPath(path string) {
	t.Path = path;
	t.Init();
}

func (t * Traverser2) IsAbs(abs bool){
	t.abs = abs;
}

func (t * Traverser2) grabRoot(){
	t.rootPulled = true;
	if t.abs || t.Root == "" {
		file := NewFile();
		file.Id = "0";
		file.Uid = t.Uid;
		t.push( file );
		t.pos++;
	}else{
		file := NewFile();
		file.Id = t.Root;
		result := FS.Find(file);
		if result != nil{
			t.rootPulled = true;
			t.push(result);
			t.pos++;
		}
	}
}

func (t * Traverser2) parse(){
	ar := strings.Split(t.Path, "/");
	
	if t.Path[0] == '/' {
		ar = ar[1:];
		//if !t.abs { t.abs = true; }
		t.abs = true;
	}
	
	newAr := make([]Pa2, 0, len(ar));
	for i := 0; i < len(ar);i++ {
		//fmt.Println("T ", ar[i]);
		//if len(ar) == 1 {
		//	newAr = newAr[:i+1]
		//	newAr[i] = Pa2{Type: "dir", Name:""};
		//	break;
		//}
		
		if ar[i] == "" {continue;}
		p := Pa2{};
		if i + 1 < len(ar) {
			p.Type = "dir"; 
		}else { p.Type = "file"; }
		
		if ar[i] == "." {
			//p.Name = "<this>";
			continue;
		}else if ar[i] == ".." {
			p.Back = true;
		}else{
			p.Name = ar[i];
		}
		newAr = newAr[:i+1];
		newAr[i] = p;
	}
	//fmt.Println(newAr);
	t.arPath = newAr;
	t.arLen = len(t.arPath);
}

func (t * Traverser2) push(file * File){
	c := cap(t.queue);
	l := len(t.queue);
	if l + 1 > c {
		tmp := t.queue;
		t.queue = make([]*File, c+10);
		copy(t.queue, tmp);
	}
	t.queue = t.queue[:l+1];
	t.queue[l] = file;
}

//func (t * Traverser2) append(file * File){
//	t.queue = t.queue
//}

func (t * Traverser2) back() * File{
	if t.pos > 1{
		t.pos--;
		t.counter++;
		return t.queue[t.pos-1];
	}else{
		
		if(t.queue[0].Id == "0"){
			t.counter++;
			return t.queue[0];
		}
		file := NewFile();
		file.Id = t.queue[0].Parent;
		//file.Id = t.Uid;
		result := FS.Find(file);
		if file != nil {
			t.prepend(result);
			//t.pos--;
			t.counter++;
			return result;
		}
	}
	return nil;
}

func (t * Traverser2) prepend(file * File){
	tmp := make([]*File, 1, 1);
	tmp[0] = file;
	t.queue = append(tmp, t.queue...);
}

func ( t * Traverser2) PrintQueue() {
	for _, f := range t.queue {
		fmt.Print("'", f.Name, "' ");
	}
	fmt.Println();
}

func (t * Traverser2) LastAttempt()  *File {
	return t.lastAttempt;
}

func (t * Traverser2) EOD() bool {
	return t.counter >= t.arLen
}

func(t * Traverser2) Incr() {
	if t.counter <= t.arLen {
		t.counter++;
	}
}

func(t * Traverser2) IncrPos(){
	t.pos++;
}

func (t * Traverser2) Last() *File{
	return t.queue[t.pos-1];
}

func (t * Traverser2) Next2() * File {
	if !t.rootPulled { 
		t.grabRoot();
		return t.queue[0];
	}
	if t.counter >= t.arLen { return nil; }
	
	queueLen := len(t.queue);
	item := t.arPath[t.counter];
	
	if item.Back {
		return t.back();
	}else if item.Name == "." {
		return t.queue[t.pos];
	}
	
	if t.pos < queueLen {
		n := t.queue[t.pos].Name;
		if n == item.Name {  			// bug: t.queue[t.pos].Type == item.Type
			t.pos++;
			t.counter++;
			//fmt.Println("from cache");
			return t.queue[t.pos-1];
		}else{
			t.queue = t.queue[:t.pos];
			//fmt.Println("Reslice last", t.queue[len(t.queue)-1]);
		}
	}
	
	file := NewFile();
	file.Name = item.Name;
	file.Type = t.arPath[t.counter].Type;
	file.Parent = t.queue[t.pos-1].Id; 		//TODO: check first
	file.Uid = t.Uid;
	t.lastAttempt = file;
	result := FS.Find(file);
	if result != nil{
		t.push(result);
		t.pos++; t.counter++;
		//fmt.Println("Found: ", file)
		return &*result;
	}

	return nil;
}

func (t  * Traverser2) AutoRun() * File{
	var file * File = t.Next2();
	for file != nil {
		if t.EOD() { return file; }
		file = t.Next2();
	}
	return nil;
}
