package lockit

import "github.com/idada/v8.go"
import ("errors"; "math"; "net/http";)


type FileCache struct {
	buff []byte
	File * File
	scripts map[int]*v8.Script
	engine * v8.Engine
}

type Storage struct{
	textS int
	textPos int
	current int
	readPos int
	evaltype map[int]string
	lastScriptPos int
	readmode bool
	
	cache * FileCache
	scriptFile * Script
	engine * v8.Engine
	backLog string
}

func NewStorage(_cache * FileCache, res http.ResponseWriter, req * http.Request, uploads * Array, keyvalue ssmap) * Storage{
	var sto * Storage;
	if _cache != nil { //Read
		sto = &Storage{readmode: true, cache: _cache};
		sto.engine = &v8.Engine{};
		*sto.engine = *_cache.engine;
		sto.scriptFile = NewScript(res, req, _cache.File, uploads, keyvalue, sto.engine);
	}else{ //Write
		sto = &Storage{readmode: false}
		sto.scriptFile =  NewScript(nil, nil, nil, nil, nil, nil);
	}
	sto.Init();
	return sto;
}

func (e * Storage) Init(){
	if !e.readmode {
		e.cache = &FileCache{};
		e.cache.scripts = make(map[int]*v8.Script);
		e.cache.buff = make([]byte, 1024);
		e.evaltype = make(map[int]string);
		e.current = 1;
		e.cache.engine = &v8.Engine{};
		*e.cache.engine = *e.scriptFile.Engine();
	}else{
		e.current = len(e.cache.buff);
	}
	e.textS = 1024;
	e.textPos = 0;
	e.readPos = 0;
	e.lastScriptPos = -1;
}
/*
func (e * Storage) AddScript(pos int, evaltype string, s []byte){
	println(e.textPos, pos);
	e.scripts[e.textPos] = s;
	e.dashed[e.textPos] = evaltype;
}
*/
func (e * Storage) AddScript(script * v8.Script, evaltype string){
	e.cache.scripts[e.textPos] = script;
	e.evaltype[e.textPos] = evaltype;
	e.textPos++;
}

func (e * Storage) Scripts() map[int]*v8.Script {
	return e.cache.scripts;
}

func (e * Storage) ScriptType(s int) string{
	return e.evaltype[s];
}

func (e * Storage) nextScript() (int, *v8.Script) {
	index := 0;
	for i, scr := range e.cache.scripts {
		if index <= e.lastScriptPos {index++; continue; }
		e.lastScriptPos = index;
		index++;
		return i, scr;
	}
	return 0, nil
}

func (e * Storage) Buffer() []byte {
	return e.cache.buff;
}

func(e * Storage) Read(buff []byte) (int, error){
	size := cap(buff);
	
	if !e.readmode { return 0, errors.New("Not in read mode."); }
	var backLogCopy int = 0;
	backLen := len(e.backLog);
	if backLen > 0 {
		backLogCopy = copy(buff, e.backLog);
		if backLogCopy >= size {
			e.backLog = e.backLog[backLogCopy: backLen-backLogCopy];
			return backLogCopy, nil;
		}
	}
	
	if e.readPos >= e.current {
		e.readPos = 0;
		return 0, errors.New("EOF");
	}
	
	pos, scr := e.nextScript();
	if scr == nil {
		pos = cap(e.cache.buff);
	}
	
	copied2 := 0; copied := 0;
	if pos != 0 {
		copied = copyOffset(buff, e.cache.buff[e.readPos: pos], backLogCopy);
		e.readPos += copied;
	}
	
	size -= copied;
	if scr != nil {
		result := e.scriptFile.RunScript(scr);		//TODO: Check eval type;
		resultLen := len(result);
		if resultLen > size {
			diff := resultLen - size;
			e.backLog = result[diff: resultLen];
			copied2 = copyOffset(buff, []byte(result[0: diff]), copied);
		}else{
			copied2 = copyOffset(buff, []byte(result), copied);
		}
	}
	return backLogCopy+copied+copied2, nil;
}

func (e * Storage) Commit(name string){
	Config.GlobalFileCache[name] = e.cache;
}

func (e * Storage) AddText(text string){
	if e.readmode { return; }
	e.current += len(text);
	if e.current > cap(e.cache.buff) {
		inc := int(math.Max(float64(e.textS), float64(len(text))));
		tmp := make([]byte, e.current + inc);
		copy(tmp, e.cache.buff);
		e.cache.buff = tmp;
	}
	for _, a := range text {
		e.cache.buff[e.textPos] = byte(a);
		e.textPos++;
	}
}

func (e * Storage) Engine() * v8.Engine {
	return e.cache.engine;
}

func (e * Storage) SetReqAndRes(req * http.Request, res * http.ResponseWriter){
	println("storage", res);
	e.scriptFile.SetReqAndRes(req, res);
}
