package lockit

import "labix.org/v2/mgo"
import ("fmt"; "io"; "os"; "reflect";)

type GoFileSystem struct{
	Mongo * mgo.Session
	Col * mgo.Collection
	Uid string
	tra * Traverser2
	
	scriptFile * File
	uploads * Array
	KeyValue map[string]string
}

type GoFileHandle struct{
	h * os.File
	buf []byte
	bufSize int
}

func NewGoFileSystem(script * File, form * Array, keyvalue map[string]string) * GoFileSystem {
	obj  := &GoFileSystem{
					Mongo: Config.Mongo,
					Uid: Config.Uid, 
					scriptFile: script, 
					uploads: form, 
					KeyValue: keyvalue,
			};
	//obj.Init();
	return obj;
}

func (f * GoFileHandle) Init(bufSize int){
	f.buf = make([]byte, bufSize);
	f.bufSize = bufSize;
}

func (f  * GoFileHandle) ReadBin() ([]byte, int){
	i, err := f.h.Read(f.buf);
	if err != nil {
		if err == io.EOF {
			return f.buf[:i], i;
		}
		return nil, 0;
	}
	return f.buf, i;
}

func (f * GoFileHandle) Read() (string, int){
	if f.h == nil {
		return "", 0;
	}
	i, err := f.h.Read(f.buf);
	if err != nil {
		return "", 0;
	}
	return string(f.buf[:i]), i;
}

func (f * GoFileHandle) Write(data string) int {
	i, err := f.h.WriteString(data);
	if err != nil {
		fmt.Println(err);
		return i;
	}
	return i;
}

func (f * GoFileHandle) Handle() * os.File{
	return f.h;
}

func(f * GoFileHandle) Zero() bool{
	err := f.h.Truncate(0);
	if err != nil{
		fmt.Println(err);
		return false;
	}
	return true;
}

func (f * GoFileSystem) SetPath(name string){
	if f.tra == nil {
		f.tra = NewTraverser2(f.Mongo, name, f.scriptFile.Parent) 
	}else{
		f.tra.SetPath(name);
	}
}

func(f * GoFileSystem) FileById(id string) * File{
	file := NewFile();
	file.Id = id; file.Uid = Config.Uid; file.Host = f.scriptFile.Host;
	return file.Get();
}

func (f * GoFileSystem) Open(name, mode string, bufSize int) *GoFileHandle{
	f.SetPath(name);
	var file * File = f.tra.Next2();
	for file != nil {
		if file.Type == "file" {
			return f.OpenById(file.Id, mode, bufSize);
		}
		file = f.tra.Next2();
	}
	return nil;
}

func (f * GoFileSystem) getMode(mode string) int {
	switch mode {
	 	case "r":
	 	return os.O_RDONLY;
		case "w":
		return os.O_WRONLY;
		case "a":
		return os.O_APPEND;
		case "wa":
		return os.O_RDWR|os.O_APPEND;
		case "wt":
		return os.O_RDWR|os.O_TRUNC;
		default:
		return os.O_RDONLY
	}
}

func (f * GoFileSystem) OpenById(id, _mode string, bufSize int) * GoFileHandle{
	file := NewFile();
	file.Id = id;
	file.Uid = Config.Uid;
	result := file.Get();
	if result == nil {
		return nil;
	}
	util := NewUtil();
	path := util.ToPath(result.Host, result.Id);

	fi, err := os.OpenFile(path, f.getMode(_mode), 0666);
	if err != nil{
		println("Failed to open file. @", result.Id, result.Name);
		return nil;
	}
	handle := &GoFileHandle{h: fi};
	handle.Init(bufSize);
	return handle;
}

func (f * GoFileSystem) Create(path string, fH  io.Reader, rt string) error{
	tra := NewTraverser2(f.Mongo, path, f.scriptFile.Parent);
	rand := Config.Random;
	var file * File = tra.Next2();
	for file != nil {	
		file = tra.Next2();
	}
	
	for !tra.EOD() {
		if file != nil { 
			file = tra.Next2();
			continue;
		}
		var err error;
		temp := NewFile();
		temp.Id = rand.Random(20);
		temp.Parent = tra.Last().Id;
		temp.Name = tra.LastAttempt().Name;
		temp.Uid = tra.Last().Uid;
		temp.Type = tra.LastAttempt().Type;
		temp.Host = tra.Last().Host;
		if temp.Type == "file"{
			if rt != ""{
				temp.ReturnType = rt;
			}
			//err = temp.Create(fH);
			err = FS.Create(temp, fH);
		}else{
			err = FS.Create(temp, nil);
			//err = temp.Create(nil);
		}
		if err != nil {  return err; }
		temp2 := *temp;
		tra.push(&temp2);
		tra.IncrPos();
		tra.Incr()
		
		file = tra.Next2();
	}
	return nil;
	//return errors.New("Path does not exist: " + path);
}

func (f * GoFileSystem) ListById(id string) (bool, *Array) {
	if id == "" {
		return true, nil;
	}
	arr := NewArray();
	file := NewFile();
	file.Uid = f.Uid;
	file.Host = f.scriptFile.Host;
	file.Parent = id;
	//file.Type = "dir";
	iter := file.Iter();
	var temp File;
	for iter.Next(&temp){
		arr.Push(temp);
	}
	return false, arr;
}

func (f * GoFileSystem) Exist(path string) *File {
	tra := NewTraverser2(f.Mongo, path, f.scriptFile.Parent);
	return tra.AutoRun();
}

func (f * GoFileSystem) List(path string) (bool, *Array){
	var result * File
	var tra * Traverser2;
	if path == "/" {
		result = NewFile();
		result.Id = "0";
	}else{
		tra = NewTraverser2(f.Mongo, path, f.scriptFile.Parent);
		result = tra.AutoRun();
	}
	
	if result != nil {
		arr := NewArray();
		if result.Type == "file" {
			return true, nil;
		}
		file := NewFile();
		
		file.Uid = result.Uid;
		file.Host = result.Host;
		file.Parent = result.Id;
		//file.Type = "dir";
		
		iter := FS.Iter(file);
		var temp * File;
		for iter.Next(&temp){
			arr.Push(*temp);
		}
		return false, arr;
	}
	return true, nil;
}

func (f * GoFileSystem) Delete(path string) bool{
	tra := NewTraverser2(f.Mongo, path, f.scriptFile.Parent);
	check := NewArray();
	var ch interface{} = tra.AutoRun();
	
	for reflect.ValueOf(ch).IsValid() && !reflect.ValueOf(ch).IsNil() {
		var t File;
		if reflect.TypeOf(ch).String() ==  "*lockit.File" {
			t = *(ch.(*File));
		}else{
			t = ch.(File);
		}
		//fmt.Println("Removing: ", t);
		//t.SetMongo(f.Mongo);
		err := FS.Remove(&t, true);
		if err != nil {
			fmt.Println("Failed: "+ t.ToString(), err);
		}
		
		if t.Type == "dir" {
			var temp File;
			var children * File = NewFile();
			
			children.Parent = t.Id;
			children.Uid = f.Uid;
			children.Host = f.scriptFile.Host;
			//iter := children.Iter();
			iter := FS.Iter(children);
			for iter.Next(&temp) {	
				if temp.Type == "dir"{
					check.Push(temp);
				}else{
					//temp.SetMongo(f.Mongo);
					//fmt.Println("I Removing: ", temp);
					err := FS.Remove(&temp, true);
					if err != nil {
						fmt.Println("Failed: "+ temp.ToString(), err);
					}
				}
			}
			iter.Close();
		}
		ch = check.Pop()
	}
	return false;
}

func (f * GoFileSystem) Uploads() []map[string]string{
	data := f.uploads.Data();
	if data == nil{
		return nil;
	}
	files := make([]map[string]string, f.uploads.Length());
	for i, f := range data{
		file := f.(*FileUpload);
		ff := make(map[string]string);
		ff["id"] = file.Id;
		ff["name"] = file.Header.Filename;
		ff["field"] = file.FieldName;
		files[i] = ff;
	}
	return files;
}

func (f * GoFileSystem) Commit(fileid, path, rt string) bool{
	data := f.uploads.Data();
	
	var final * FileUpload;
	if len(data) <= 0 {
		println("no uploads to commit!");
		return false;
	}
	
	if path[ len(path)-1] == '/' {
		println("path is directory not file");
		return false;
	}
	
	for _, f := range data{
		file := f.(*FileUpload);
		if fileid == file.Id{
			final = file;
			println("Commiting: "+ fileid + " " + file.Header.Filename);
		}
	}
	if final == nil{ return false; }
	
	//pos := strings.LastIndex(path, "/");
	//dirPath := path[0: pos+1];
	
	fH, err := final.Header.Open();
	
	if err != nil{
		println("couldn't open tmp file");
		print(err);
		return false;
	}
	err2 := f.Create(path, fH, rt);
	
	if err2 != nil {
		return false;
	}
	
	return true;
}

func (f * GoFileSystem) Keys() map[string]string {
	keys := make(map[string]string);
	for k, v := range f.KeyValue {
		keys[k] = v;
	}
	return keys;
}

func (f * GoFileSystem) Key(key string) string{
	for k, v := range f.KeyValue {
		if k == key{ return v; }
	}
	return ""
}
